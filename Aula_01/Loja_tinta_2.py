# Programa para uma loja de tinta que simula um orçamento.

# 1 litro para cada 6 metros
# latao de 18: 80 reais
# latao de 4L: 25 reais
# a) apenas 18L; b) apenas 4L; c) mistura

# Importar a função ceiling (arrendondamento para cima)
import math
import numpy as np

# input dos usuários

while True:
    try:
        area = float(input('Informe o tamanho da área a ser pintada (em metros quadrados): '))
        break
    except ValueError:
        print('Oops...Valor da área inválido. \nDigite 10 caso deseje pintar uma área de dez metros quadrados.')

# Valor das tintas
preco_18litros = 80.00
preco_4litros = 25.00

# Rendimento das tintas

rendimento = 6

rendimento_18litros = 18*rendimento
rendimento_4litros = 4*rendimento

# Orçamento A: apenas latão de 18L:

quantidade_18 = math.ceil(area / rendimento_18litros)

# Custo total
custo_orcamento_a = round(quantidade_18*preco_18litros,2)

# Orçamento B: apenas latas de 4L

quantidade_4 = math.ceil(area / rendimento_4litros)

# Custo total
custo_orcamento_b = round(quantidade_4*preco_4litros,2)


# Menor custo
menor_custo = min(custo_orcamento_a, custo_orcamento_b)



# Orçamento_C: mistura entre tintas

# Temos um problema do tipo:
# 108x + 24y = area
#  80x + 25y = custo_total

# Area é dada pelo usuário. Custo_total vou assumir como o min(custo_a, custo_b).
# O x e y encontrados serão arrendodados para cima.

a = np.array([[rendimento_18litros, rendimento_4litros],[preco_18litros,preco_4litros]])
b = np.array([area, menor_custo])

solucao = np.linalg.solve(a,b)

quantidade_18_c = math.ceil(solucao[0])
quantidade_4_c = math.ceil(solucao[1])

custo_orcamento_c = quantidade_18_c*preco_18litros + quantidade_4_c*preco_4litros


# Orçamento
print('________________________________________________________')
print('Abaixo os detalhes do seu orçamento: \n')

print('Orçamento A: Apenas latões de 18 Litros.\n')
print('Área pintada: {} m² \nNúmero de tintas:  {} lata(s) de 18 litros \nRendimento de cada tinta: {} metros quadrados \nPreço unitário: R$ {} \nCusto total: R$ {}'.format(area,int(quantidade_18),rendimento_18litros,preco_18litros,custo_orcamento_a))
print('________________________________________________________')

print('Orçamento B: Apenas latões de 4 Litros.\n')
print('Área pintada: {} m² \nNúmero de tintas:  {} lata(s) de 4 litros \nRendimento de cada tinta: {} metros quadrados \nPreço unitário: R$ {} \nCusto total: R$ {}'.format(area,int(quantidade_4),rendimento_4litros,preco_4litros,custo_orcamento_b))
print('________________________________________________________')

print('Orçamento C: Mistura entre os dois tipos de tinta.\n')
print('Área pintada: {} m² \nNúmero de tintas:  {} lata(s) de 4 litros e {} de 18 litros \nRendimento de cada tinta: {} m2 (4L) e {} m2 (18L) \nPreço unitário: R$ {} (4L) e R$ {} (18L) \nCusto total: R$ {}'.format(area,int(quantidade_4_c),int(quantidade_18_c),rendimento_4litros,rendimento_18litros,preco_4litros,preco_4litros,custo_orcamento_c))
print('________________________________________________________')


# Sugestão:

if custo_orcamento_a == min(custo_orcamento_a,custo_orcamento_b,custo_orcamento_c):
    print("Sugestão: Comprar {} tinta(s) de 18L sai mais em conta.".format(quantidade_18))
elif custo_orcamento_b == min(custo_orcamento_a,custo_orcamento_b,custo_orcamento_c):
    print("Sugestão: Comprar {} tinta(s) de 4L sai mais em conta.".format(quantidade_4))
else:
    print("Sugestão: Comprar {} lata(s) de 18L e {}  de 4L sai mais em conta.".format(quantidade_18_c, quantidade_4_c))

print('________________________________________________________')
