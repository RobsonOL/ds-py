# Programa que simula financiamento de um veículo.


# Valores a serem informados pelo usuário. O programa produz um erro específico se o usuário não informa o valor no formato adequado.
while True:
    try:
        preco = round(float(input('Informe o valor total do automóvel: ')),2)
        entrada = round(float(input('Informe o valor da entrada: ')),2)

        break
    except ValueError:
        print('Oops...O preço precisa ser informado como um valor númerico. \nUse o ponto como um separador decimal. Exemplo: 40900.50')


while True:
    try:
        taxa = round(float(input('Qual a taxa de juros mensal do financiamento? (em %) ')),2)
        break
    except ValueError:
        print('Oops...Informe a taxa de juros em porcentagem. Exemplo: 2')

while True:
    try:
        parcelas = int(input('Insira o número de parcelas (em meses): '))
        break
    except ValueError:
        print('O número de meses deve ser informado como um número inteiro. Exemplo: 60.')

# fórmula para o valor do pagamento.

valor_financiado = preco - entrada

pmt = round(valor_financiado*((((1+(taxa/100))**parcelas)*(taxa/100))/((1+(taxa/100))**parcelas - 1)),2)

valor_total = round(entrada + parcelas*pmt,2)

total_juros = round(parcelas*pmt - valor_financiado,2)

# print final

print('-----------------------------')
print('Consulte abaixo as informações do financiamento do seu veículo: \n')

print('Valor total do veículo: R$ {} \nPrazo de financiamento: {} meses \nTaxa mensal de juros: {}% a.m. \nValor da prestação: R$ {} \nValor total: R$ {} \nValor total de juros: R${}'.format(preco, parcelas, taxa, pmt, valor_total, total_juros))
print('-----------------------------')
