# Jogo de pedra,papel e tesoura

from random import randrange

print('Escolha Pedra (0), Papel(1) ou Tesoura(2)')

jogadas = ['Pedra','Papel','Tesoura']

while True:
    try:
        jogador = int(input('Escolha sua jogada (0,1 ou 2): '))
    except ValueError:
        print('Algo errado!')
    computador = randrange(3)
    if jogadas[jogador] == jogadas[computador]:
        print('Jogador: {} x \nComputador: {}.\n Empate!'.format(jogadas[jogador], jogadas[computador]))
    elif jogadas[jogador] == 'Pedra' and jogadas[computador] == 'Papel':
        print('Jogador: {} x \nComputador: {}.\n Você perdeu!'.format(jogadas[jogador], jogadas[computador]))
    elif jogadas[jogador] == 'Pedra' and jogadas[computador] == 'Tesoura':
        print('Jogador: {} x \nComputador: {}.\n Você venceu!'.format(jogadas[jogador], jogadas[computador]))
    elif jogadas[jogador] == 'Tesoura' and jogadas[computador] == 'Papel':
        print('Jogador: {} x \nComputador: {}.\n Você venceu!'.format(jogadas[jogador], jogadas[computador]))
    elif jogadas[jogador] == 'Tesoura' and jogadas[computador] == 'Pedra':
        print('Jogador: {} x \nComputador: {}.\n Você perdeu!'.format(jogadas[jogador], jogadas[computador]))
    elif jogadas[jogador] == 'Papel' and jogadas[computador] == 'Tesoura':
        print('Jogador: {} x \nComputador: {}.\n Você perdeu!'.format(jogadas[jogador], jogadas[computador]))
    else:
        print('Jogador: {} x \nComputador: {}.\n Você perdeu!'.format(jogadas[jogador], jogadas[computador]))

