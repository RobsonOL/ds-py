# Programa para uma loja de tinta que simula um orçamento.

import math

# cada litro rende 3 metros. Logo, um latão de 18 litros rende 18*3
while True:
    try:
        area = float(input('Informe o tamanho da área a ser pintada (em metros quadrados): '))
        break
    except ValueError:
        print('Oops...Valor da área inválido. Digite 10 se deseja pintar 10 metros quadrados.')

# Preço da tinta
preco = 80.00

# Rendimento da tinta

rendimento = 18*3

# Quantidade  de tintas

quantidade = math.ceil(area/rendimento)


# Custo total
custo = round(quantidade*preco,2)

# Orçamento
print('---------------------------------------')
print('Abaixo os detalhes do seu orçamento: \n')
print('Área pintada: {} m² \nNúmero de tintas:  {} latão(ões) de 18 litros  \nRendimento de cada latão: {} metros quadrados \nPreço unitário: R$ {} \nCusto total: R$ {}'.format(area,int(quantidade),rendimento,preco,custo))
print('---------------------------------------')
