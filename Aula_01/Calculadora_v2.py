# Criando uma classe que calcula o financiamento e produz um relatório para o orçamento

class Financiamento:
    def __init__(self):
        self.nome = input('Informe seu nome: ')

        while True:
            try:
                self.preco = float(input('Informe o preço do produto: '))
                break
            except ValueError:
                print('Oops...tente informar um valor válido.')
        while True:
            try:
                self.entrada = float(input('Digite o valor da sua entrada: '))
                break
            except ValueError:
                print("Oops...tente informar um valor válido.")

        while True:
            try:
                self.juros = float(input('Informe o juros do financiamento (em %): '))
                break
            except ValueError:
                print('Oops...Informe o valor em porcentagem.')
        while True:
            try:
                self.parcelas = int(input('Digite o número de parcelas (em meses): '))
                break
            except ValueError:
                print('Oops...Tente um número inteiro para os meses.')

        self.valor_financiado = self.preco - self.entrada

        self.prestacao = round(self.valor_financiado * (
                    (((1 + (self.juros / 100)) ** self.parcelas) * (self.juros / 100)) / (
                        (1 + (self.juros / 100)) ** self.parcelas - 1)), 2)

        self.valor_total = round(self.entrada + self.parcelas * self.prestacao, 3)

        self.total_juros = round(self.parcelas*self.prestacao - self.valor_financiado,3)

    def Relatorio(self):
        print('___________________________________')
        print('{}, consulte abaixo as informações do financiamento do seu veículo: \n'.format(self.nome))

        print(
            'Valor total do veículo: R$ {} \nPrazo de financiamento: {} meses \nTaxa mensal de juros: {}% a.m. \nValor da prestação: R$ {} \nValor total: R$ {} \nValor total de juros: R${}'.format(
                self.preco, self.parcelas, self.juros, self.prestacao, self.valor_total, self.total_juros))
        print('___________________________________')

# Produzir um orcamento:
orc1 = Financiamento()
orc1.Relatorio()




