from random import randrange
print('Vamos produzir um número escondido entre 1 e n. Tente adivinhar.')

# Gerar o número aleatório
while True:
    try:
        n = int(input('Escolha o valor de n: '))
        hidden_number = randrange(n-1)
        print('Número gerado!')
        break
    except ValueError:
        print('Este não é um número inteiro válido.')

# Perguntar ao usuário o número que deseja chutar

while True:
    try:
        guess = int(input('Escolha um número entre 1 e {}: '.format(n)))
        if guess == hidden_number:
            print('Parabéns!')
        elif (guess - hidden_number)>0:
            print('Tente um número menor!')
        else:
            print('Tente um número maior!')
    except ValueError:
        print('Este não é um número inteiro válido.')
