# Classe: o diagrama que vamos usar para criar instâncias
# Instâncias são cada orçamento.
# Variáveis: são os dados sobre cada instância, como nome do cliente, quantidade, etc.
import math


# Classe para criação de tintas pela loja
class Tinta:
    rendimento_litro = 6.0

    def __init__(self, produto, litro, preco):
        self.produto = produto
        self.litro = int(litro)
        self.preco = float(preco)
        self.rendimento = float(litro * self.rendimento_litro)

    # rendimento_litro é uma variável de classe (compartilhada entre todas as instâncias)
    def mudar_rendimento(self):
        self.rendimento = float(self.litro * rendimento_litro)

    def mostrar_informacoes(self):
        print('Tinta {} cadastrada. \nPreço: R$ {} \nLitro: {} L \nRendimento: {} m2'.format(self.produto, self.preco,
                                                                                             self.litro,
                                                                                             self.rendimento))


# Criando dois tipos de tintas: de 18L e de 4L

# Fixando o rendimento de todas as tinta em 6m2/L
Tinta.mudar_rendimento = 6.0

tinta_18 = Tinta('18litros', 18, 80)
tinta_4 = Tinta('4litros', 4, 25)


# Classe para criação de orçamento pelo usuário

class Orcamento:
    def __init__(self):
        self.usuario = input('Informe seu nome: ')
        self.area = float(input('Informe a área a ser pintada (em m2): '))
        self.quant_18_apenas = math.ceil(self.area / tinta_18.rendimento)
        self.quant_4_apenas = math.ceil(self.area / tinta_4.rendimento)
        self.custo_18 = self.quant_18_apenas * tinta_18.preco
        self.custo_4 = self.quant_4_apenas * tinta_4.preco

    def mostrar_quantidade(self):
        return self.quant_18_apenas
        return self.quant_4_apenas
    def relatorio(self):
        print('_______________________')
        print('Orcamento A:')
        print(
            'Área: {} m2 \nNúmero de tintas: {} latas \nRendimento individual: {} \nPreço unitário: R$ {} \nCusto total: R$ {}'
            .format(self.area, self.quant_18_apenas, tinta_18.rendimento, tinta_18.preco, self.custo_18)
        )

        print('_______________________')
        print('Orcamento B:')
        print(
            'Área: {} m2 \nNúmero de tintas: {} latas \nRendimento individual: {} \nPreço unitário: R$ {} \nCusto total: R$ {} '.format(
                self.area, self.quant_4_apenas, tinta_4.rendimento, tinta_4.preco, self.custo_4))


# Iniciar um orçamento:
orc1 = Orcamento()

# Produzir um relatório de orçamento
orc1.relatorio()
