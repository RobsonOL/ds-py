# Programa que informa o salário mensal do usuário:

class Salario:
    def __init__(self):
        while True:
            try:
                self.nome = input('Informe seu nome: ')
                self.mes = input('Informe o mês atual (exemplo: Janeiro).')
                self.salario = float(input('Qual seu salário-hora (em R$): '))
                self.horas = int(input('Informe o número de horas trabalhadas este mês: '))
                break
            except ValueError:
                print('Valor não válido.')

        self.salario_mes = round(self.salario*self.horas,3)

    def Relatorio(self):
        print('FOLHA DE PAGAMENTO:')
        print('{}, seu salário em {} foi de R$ {}.'.format(self.nome, self.mes, self.salario_mes))

# Exemplo:
joao = Salario()
joao.Relatorio()