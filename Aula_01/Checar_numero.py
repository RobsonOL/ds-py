# Checar se número inputado é negativo, positivo ou zero.

while True:
    try:
        numero = float(input('Insira um número: '))
        break
    except ValueError:
        print('Valor não válido.')

if numero < 0:
    print('O número é negativo.')
elif numero == 0:
    print('O numero é igual a zero.')
else:
    print('O número é positivo.')
