# Programa que checa se o número informado é par ou impar
while True:
    try:
        numero = float(input('Insira um número: '))
        break
    except ValueError:
        print('Insira um valor válido.')


if numero%2 == 0:
    print('O número é par.')
else:
    print('O número é impar.')