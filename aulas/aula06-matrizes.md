![ds-py-capa.png](attachment:ds-py-capa.png)


# Aula 06 - Vetores e Matrizes


## Objetivo
Desenvolver operações básicas de vetores e matriz no `Python`.


## Conteúdo
1. [Definição](#intro)
2. [Entrada de Matrizes](#matrix)
3. [Operações básicas em matrizes](#operacoes)
4. [Estatísticas básicas](#stats)
5. [Reshape](#reshape)
6. [Operações de álgebra linear](#linalg)
7. [Gerador de Valores Aleatórios](#random)
8. [Exercícios](#exerc)

<a name='intro'></a>
# Vetores e Matrizes

- Matrizes são **arranjos numéricos** com diversas aplicações em matemática, estatística, econometria...
- Um vetor (*array*) pode ser definido como uma matriz de apenas uma dimensão ou uma matriz pode ser defida como o empilhamento de um ou mais vetores.
- A biblioteca `numpy` possui uma série de funcões específicas para vetores e matrizes.

- Normalmente, usamos o termo **matriz** quando há mais de uma dimensão

\begin{equation}
A=
\begin{bmatrix}
    x_{11}       & x_{12} & x_{13} & \dots & x_{1m} \\
    x_{21}       & x_{22} & x_{23} & \dots & x_{2m} \\
    \vdots & \vdots & \vdots & \ddots & \vdots\\
    x_{n1}       & x_{n2} & x_{n3} & \dots & x_{nm}\\
\end{bmatrix}
\end{equation}

## Especificações `numpy` de vetores e matrizes

- Toda matriz deve estar contida entre `[]` e depois entre `()`, i.e., `matrix([...])`
- Cada linha é formada por uma lista
- Todo vetor linha ou coluna pode ser escrito usando `array(...)`. Caso queiramos gerar uma matriz: `array([...])`

## Vetor

- Vamos escrever o seguinte vetor linha:
\begin{equation}
A=
\begin{bmatrix}
    1 & 2 & 3\\
\end{bmatrix}
\end{equation}


```python
# Carregando a biblioteca
import numpy as np

# Criando A
A = np.array([1, 2, 3])
print(A)
```

    [1 2 3]


- Vamos escrever o seguinte vetor coluna:
\begin{equation}
B=
\begin{bmatrix}
    1\\
    2\\
    3\\
\end{bmatrix}
\end{equation}


```python
# Criando B
B = np.array([[1],
              [2],
              [3]])

print(B)
```

    [[1]
     [2]
     [3]]


## Consulta e Fatiamento de vetores


```python
# Criando um vetor 
x = np.array(range(7,0,-1))
x[:]
```




    array([7, 6, 5, 4, 3, 2, 1])




```python
x[0] # primeiro valor do vetor
```




    7




```python
# Selecionando todos os valores até o 3º elemento
x[:3]
```




    array([7, 6, 5])




```python
# Selecionando todos os valores depois do 3º elemento
x[3:]
```




    array([4, 3, 2, 1])




```python
# Selecionando o último elemento
x[-1]
```




    1



## Operações básicas com vetores

- Adição
- Subtração
- Multiplicação


```python
a = np.array([1, 2, 3])
b = np.array([0, 1, 1])
print(f' A = {a}\n B = {b}')
```

     A = [1 2 3]
     B = [0 1 1]



```python
a + b
```




    array([1, 3, 4])




```python
a - b
```




    array([1, 1, 2])




```python
a * b
```




    array([0, 2, 3])



<a name='matrix'></a>
# Entrada de Matrizes

- Vamos escrever o seguinte vetor coluna:
\begin{equation}
A=
\begin{bmatrix}
    1 & 2 & 3\\
    4 & 5 & 6 \\
    7 & 8 & 9\\
\end{bmatrix}
\end{equation}


```python
A = np.matrix([[1, 2, 3], 
               [4, 5, 6],
               [7, 8, 9]])
A
```




    matrix([[1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]])




```python
# Criando matriz usando ARRAY
A = np.array([[1, 2, 3], 
                   [4, 5, 6],
                   [7, 8, 9]])
A
```




    array([[1, 2, 3],
           [4, 5, 6],
           [7, 8, 9]])




```python
# Imagine o caso em que temos listas 
a = [1,2,3]
b = [4,5,6]
c = [7,8,9]

A = np.asarray([a,b,c])
A
```




    array([[1, 2, 3],
           [4, 5, 6],
           [7, 8, 9]])



## Consulta e Fatiamento de matrizes


```python
# selecionar primeiro elemento
A[0,0]
```




    1




```python
# Duas primeiras linhas e todas as colunas da matriz
A[:2,:]
```




    array([[1, 2, 3],
           [4, 5, 6]])




```python
# Todas as linhas e a segunda coluna
A[:,1]
```




    array([2, 5, 8])



## Descrevendo uma matriz

- Podemos descrever o `shape`, `size` e `dim` da matriz.


```python
A = np.array([
    [1, 2, 3], 
    [4, 5, 6],
    [7, 8, 9]
])

# Linhas x Colunas
A.shape
```




    (3, 3)




```python
# Número de elementos = linhas * colunas
A.size
```




    9




```python
# Número de dimensões
A.ndim
```




    2



### Matrizes especiais



```python
#Matriz com valores zerados
ZERO = np.zeros((4,3), 'int')
ZERO
```




    array([[0, 0, 0],
           [0, 0, 0],
           [0, 0, 0],
           [0, 0, 0]])




```python
#Matriz com valores 1
UM = np.ones((4,3), 'float')
UM
```




    array([[1., 1., 1.],
           [1., 1., 1.],
           [1., 1., 1.],
           [1., 1., 1.]])




```python
#Matriz com valores repetidos
np.ones((4,3), 'int')*5
```




    array([[5, 5, 5],
           [5, 5, 5],
           [5, 5, 5],
           [5, 5, 5]])




```python
#Matriz identidade
I = np.identity((10), 'int')
I
```




    array([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]])



<a name='operacoes'></a>
# Operações básicas em matrizes



```python
A = np.array([[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9]])
A
```




    array([[1, 2, 3],
           [4, 5, 6],
           [7, 8, 9]])




```python
# Somar 100 a todos os elementos da matriz
A + 100
```




    array([[101, 102, 103],
           [104, 105, 106],
           [107, 108, 109]])




```python
# Subtrair 100 a todos os elementos da matriz
A - 10
```




    array([[-9, -8, -7],
           [-6, -5, -4],
           [-3, -2, -1]])




```python
# Multiplicar por 10 todos os elementos da matriz
A * 10
```




    array([[10, 20, 30],
           [40, 50, 60],
           [70, 80, 90]])




```python
# Dividir por 10 todos os elementos da matriz
A / 10
```




    array([[0.1, 0.2, 0.3],
           [0.4, 0.5, 0.6],
           [0.7, 0.8, 0.9]])




```python
# Potencializar por 2 todos os elementos da matriz
A ** 2
```




    array([[ 1,  4,  9],
           [16, 25, 36],
           [49, 64, 81]])



## Ordenar valores de uma matriz


```python
A = np.array([[4, 5, 6],
              [2, 3, 1],
              [7, 8, 9]])

#Ordenada pelos valores da primeira coluna
indice = A[:,0].argsort(axis=0)
print(indice)
```

    [1 0 2]



```python
A_ordenada = A[indice, :]
A_ordenada
```




    array([[2, 3, 1],
           [4, 5, 6],
           [7, 8, 9]])



<a name='stats'></a>
# Estatísticas básicas


```python
A = np.array([[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9]])
# Máximo valor da matriz
np.max(A)
```

    [[4 5 6]
     [2 3 1]
     [7 8 9]]





    9




```python
# modo alternativo
A.max()
```




    9




```python
print(A)
# Máximo valor de cada coluna da matriz
np.max(A, axis=0)
```

    [[4 5 6]
     [2 3 1]
     [7 8 9]]





    array([7, 8, 9])




```python
# Máximo valor de cada linha da matriz
A.max(axis=1)
```




    array([6, 3, 9])




```python
print(A)
# Valor Mínimo
np.min(A)
```

    [[4 5 6]
     [2 3 1]
     [7 8 9]]





    1




```python
# Somatório
np.sum(A)
```




    45




```python
# Soma por coluna
np.sum(A, axis=0)
```




    array([13, 16, 16])




```python
print(A)
# Média
np.mean(A)
```

    [[4 5 6]
     [2 3 1]
     [7 8 9]]





    5.0




```python
np.mean(A, axis=0).round(2)
```




    array([4.33, 5.33, 5.33])




```python
# Variância
np.var(A)
```




    6.666666666666667




```python
# Variância
np.var(A, axis = 0).round(2)
```




    array([ 4.22,  4.22, 10.89])




```python
# Desvio-padrão
np.std(A)
```




    2.581988897471611




```python
# Retorna DP por coluna
A.std(axis = 0).round(1)
```




    array([2.1, 2.1, 3.3])



## Exercício 1

- Suponha que uma turma possui as seguintes notas:

|Aluno | AV1 | AV2 | AV3|
|---|---|---|---|
|A| 10 | 8 | 7 |
|B| 2 | 9 | 10 |
|C| 4 | 6 | 4 |


- Represente as notas acima em uma matriz `A`, calculando a média e desvio-padrão por aluno e por avaliação. Escreva o resultado em um dicionário.


```python
A = np.array([[10,8,7], [2,9,10], [4,6,4]])
resultado = {'aluno':{'média': np.mean(A, axis=1).round(1), 'dp': np.std(A, axis=1).round(1)},
             'unidade': {'média': np.mean(A, axis=0).round(1), 'dp': np.std(A, axis=0).round(1)}}
resultado
```




    {'aluno': {'média': array([8.3, 7. , 4.7]), 'dp': array([1.2, 3.6, 0.9])},
     'unidade': {'média': array([5.3, 7.7, 7. ]), 'dp': array([3.4, 1.2, 2.4])}}



## Exercício 2

- Suponha que uma turma possui as seguintes notas:

|Aluno | AV1 | AV2 | AV3|
|---|---|---|---|
|A| 10 | 8 | 7 |
|B| 2 | 9 | 10 |
|C| 4 | 6 | 4 |
|D|0|5|F|


- Represente as notas acima em uma matriz `A`, calculando a média e desvio-padrão por aluno e por avaliação. Trate a falta do aluno D como uma informação faltante.


```python
A = np.array([[10,8,7], [2,9,10], [4,6,4], [0,5,np.nan]])
resultado = {'aluno':{'média': np.nanmean(A, axis=1).round(1), 'dp': np.nanstd(A, axis=1).round(1)},
             'unidade': {'média': np.nanmean(A, axis=0).round(1), 'dp': np.nanstd(A, axis=0).round(1)}}
resultado
```




    {'aluno': {'média': array([8.3, 7. , 4.7, 2.5]),
      'dp': array([1.2, 3.6, 0.9, 2.5])},
     'unidade': {'média': array([4., 7., 7.]), 'dp': array([3.7, 1.6, 2.4])}}




```python
av1=resultado['unidade']['média'][0]
av2=resultado['unidade']['média'][1]
av3=resultado['unidade']['média'][2]
print(f'''
Médias da Turma
-------------------
1ª Avaliação: {av1}
2ª Avaliação: {av2}
3ª Avaliação: {av3}
''')
```

    
    Médias da Turma
    -------------------
    1ª Avaliação: 4.0
    2ª Avaliação: 7.0
    3ª Avaliação: 7.0
    


<a name='reshape'></a>
# Reshape Vatores e Matrizes

- O `reshape` permite reestruturar uma matriz para mantermos os mesmos dados, mas com uma nova organização de linhas e colunas.

- O único requisito é que o formato da matriz original e da nova contenha o mesmo número de elementos.


```python
# Criando um vetor
A = np.array([range(0,12)])
print(A)
A.shape
```

    [[ 0  1  2  3  4  5  6  7  8  9 10 11]]





    (1, 12)




```python
# Reshape 2x6
A.reshape(2, 6)
```




    array([[ 0,  1,  2,  3,  4,  5],
           [ 6,  7,  8,  9, 10, 11]])




```python
B = A.reshape(3,4)
B
```




    array([[ 0,  1,  2,  3],
           [ 4,  5,  6,  7],
           [ 8,  9, 10, 11]])




```python
# Transformar uma matriz em uma unica dimensao
# o valor -1 garante a mundança de todos os elemenos
B.reshape(1, -1)
```




    array([[ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11]])




```python
B.reshape(-1, 1)
```




    array([[ 0],
           [ 1],
           [ 2],
           [ 3],
           [ 4],
           [ 5],
           [ 6],
           [ 7],
           [ 8],
           [ 9],
           [10],
           [11]])




```python
print(B)

# Outra forma de tornar a matriz em 1-D
B.flatten()
```

    [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]





    array([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11])



## Matriz Transposta

- A transposição é uma operação comum em álgebra linear, na qual os índices de coluna e linha de cada elemento são trocados.


```python
B.T 
```




    array([[ 0,  4,  8],
           [ 1,  5,  9],
           [ 2,  6, 10],
           [ 3,  7, 11]])




```python
print(f'Matriz original\n {B}\n\n Matriz transposta\n{B.T}')
# Transpose matrix
```

    Matriz original
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]
    
     Matriz transposta
    [[ 0  4  8]
     [ 1  5  9]
     [ 2  6 10]
     [ 3  7 11]]


- OBS: tecnicamente, um vetor não pode ser transposto porque é apenas uma coleção de valores.


```python
# Vetor transposto!
np.array([1, 2, 3, 4, 5, 6]).T
```




    array([1, 2, 3, 4, 5, 6])




```python
# Um vetor declarado como matriz, porém pode ser transposto
np.matrix([[1, 2, 3, 4, 5, 6]]).T
```




    matrix([[1],
            [2],
            [3],
            [4],
            [5],
            [6]])



<a name='linalg'></a>
# Operações de álgebra linear

## Posto matricial

- O posto (rank) de uma matriz é o **número de linhas não-nulas da matriz**, quando escrita na forma escalonada por linhas. 

- Equivalentemente, corresponde ao número de linhas ou colunas linearmente independentes da matriz. 

- O posto tem implicações em relação à independência linear e a dimensão de um espaço vetorial.

- NumPy possui uma função *linear algebra*: `np.linalg.matrix_rank( )`

$$rank(A_{n,m})\leq \min(n,m)$$



```python
A = np.array([[1, 1, 1], [0, 0, 0]])

# RANK
print(A)

np.linalg.matrix_rank(A)
```

    [[1 1 1]
     [0 0 0]]





    1




```python
A = np.array([[1, 1, 1],
              [1, 1, 10],
              [1, 1, 15],
              [1, 2,  2]])
```


```python
np.linalg.matrix_rank(A)
```




    3




```python
np.linalg.matrix_rank(A.reshape(2,6))
```




    2



## Diagonal e traço de uma matriz

### Diagonal principal


```python
A = np.array([[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9]])

## Diagonal principal
A.diagonal()
```




    array([1, 5, 9])




```python
print(A)
# A diagonal logo acima da diagonal principal
A.diagonal(offset=1)
```

    [[1 2 3]
     [4 5 6]
     [7 8 9]]





    array([2, 6])




```python
# A Diagonal logo abaixo da diagonal principal
A.diagonal(offset=-1)
```




    array([4, 8])




```python
# A Diagonal mais distante abaixo da diagonal principal
A.diagonal(offset=-2)
```




    array([7])



# Traço de uma matriz

- O traço de uma matriz é a soma dos valores dos elementos da diagonal principal


```python
print(A)
sum(A.diagonal())
```

    [[1 2 3]
     [4 5 6]
     [7 8 9]]





    15




```python
# Usando a função trace
A.trace()
```




    15



# Determinante de uma matriz

- O determinante é calculado para uma matriz quadrada: $A_{nn}$.
- Por exemplo, o determinante de uma matriz $2 \times 2$ é:
\begin{equation}
    \begin{vmatrix}
        a & b\\
        c & d\\
    \end{vmatrix} = ad - bc    
\end{equation}

- Uma expressão geral, para quaisquer $n$ é dada por:

$$|A|=\sum_{i=1}^k a_{ij} C_{ij},$$
onde $C_{ij}=(-1)^{i+j}M_{ij}$ é o cofator de $A_{ij}$ e $M_{ij}$ é uma matrix formada pela exclusão da linha $i$ e coluna $j$ de $A$.

## Exercício 

Dada a matriz 

\begin{equation}
    A = 
    \begin{bmatrix}
        1 & 2\\
        1 & 4\\
    \end{bmatrix},
\end{equation}

calcule o determinante.


```python
# Modo 1: usando o traço

A = np.array([[1, 2],
              [1, 4]])

a = A.trace() #Soma da diagonal principal
b = np.fliplr(A).trace() #Soma da anti-diagonal
a - b
```




    2




```python
# Modo 2: usando função LinAlg

A = np.array([[1, 2],
              [1, 4]])

# Determinante
np.linalg.det(A)
```




    2.0




```python
# Uma nova matriz
B = np.array([[1, 2, 3, 4],
              [2, 4, 6, 0],
              [3, 8, 9, 2],
              [1, 1, 2, 1]])

# Determinante
np.linalg.det(B).round(2)
```




    16.0



# Operações matemáticas diversas

## Adição e subtração



```python
A = np.array([[1, 1, 1],
              [1, 1, 1],
              [1, 1, 2]])

B = np.array([[1, 3, 1],
              [1, 3, 1],
              [1, 3, 8]])
```


```python
np.add(A, B) # Adição
```




    array([[ 2,  4,  2],
           [ 2,  4,  2],
           [ 2,  4, 10]])




```python
A + B
```




    array([[ 2,  4,  2],
           [ 2,  4,  2],
           [ 2,  4, 10]])




```python
np.subtract(A, B)
```




    array([[ 0, -2,  0],
           [ 0, -2,  0],
           [ 0, -2, -6]])




```python
A - B
```




    array([[ 0, -2,  0],
           [ 0, -2,  0],
           [ 0, -2, -6]])



## Multiplicação matricial

$$C(i,j)=\sum_{k=1}^K A(i,k)B(k,j)$$


```python
print(f'A =\n {A} \n\n B = \n {B}')
```

    A =
     [[1 1 1]
     [1 1 1]
     [1 1 2]] 
    
     B = 
     [[1 3 1]
     [1 3 1]
     [1 3 8]]



```python
np.dot(A, B)
```




    array([[ 3,  9, 10],
           [ 3,  9, 10],
           [ 4, 12, 18]])




```python
A @ B
```




    array([[ 3,  9, 10],
           [ 3,  9, 10],
           [ 4, 12, 18]])




```python
print(f'A =\n {A} \n\n B = \n {B}')

# E se ao invés de @ usássemos *:
A * B
```

    A =
     [[1 1 1]
     [1 1 1]
     [1 1 2]] 
    
     B = 
     [[1 3 1]
     [1 3 1]
     [1 3 8]]





    array([[ 1,  3,  1],
           [ 1,  3,  1],
           [ 1,  3, 16]])



# Inversão matricial

- O inverso de uma matriz quadrada, $A_{n,n}$, é uma segunda matriz $A^{-1}$, tal que:

$$A A^{-1} = I$$

- Qual seria a matriz inversa de W:

\begin{equation}
    W = 
    \begin{bmatrix}
        1 & 4\\
        2 & 5\\
    \end{bmatrix},
\end{equation}




```python
W = np.matrix([[1, 4],
              [2, 5]])

np.linalg.inv(W).round(2)
```




    array([[-1.67,  1.33],
           [ 0.67, -0.33]])




```python
# Vamos demonstrar a definição que a multiplicação de A por sua inversa ser igual a matriz identidade

# Multiply matrix and its inverse
W @ np.linalg.inv(W)
```




    matrix([[1., 0.],
            [0., 1.]])



## Autovalores e autovetores

$$ A v = \lambda v$$

A é uma matriz quadrada, $\lambda$ contêm os autovalores e $v$ os autovetores.




```python
A = np.array([[1, -1, 3],
              [1, 1, 6],
              [3, 8, 9]])

# Calculando...
autovalor, autovetor = np.linalg.eig(A)
```


```python
autovalor
```




    array([13.55075847,  0.74003145, -3.29078992])




```python
autovetor
```




    array([[-0.17622017, -0.96677403, -0.53373322],
           [-0.435951  ,  0.2053623 , -0.64324848],
           [-0.88254925,  0.15223105,  0.54896288]])



<a name='random'></a>
# Gerador de Valores Aleatórios



```python
# Configurando a semente (seed)
np.random.seed(0)

# 3 Valores entre 0.0 and 1.0
np.random.random(3)
```




    array([0.5488135 , 0.71518937, 0.60276338])




```python
# 5 valores inteiros entre 1 e 10
np.random.seed(0)
np.random.randint(0, 11, 5)
```




    array([5, 0, 3, 3, 7])




```python
# 5 valores inteiros entre 1 e 10: sem SEED(), esse valor se altera a cada execução
np.random.randint(0, 11, 5)
```




    array([ 8, 10,  1,  6,  7])




```python
# Número a partir de uma distribuição Z, com média 0.0 e dp = 1.0
np.random.normal(0, 1, 3)
```




    array([2.54520078, 1.08081191, 0.48431215])



<a name='exerc'></a>
# Exercício 3

## Solução de sistemas de equações

Resolva o seguinte sistema de equação no Python.

\begin{equation}
\left\{
\begin{array}{r}
2x-5y=11\\
3x+6y=3
\end{array}
\right.
\end{equation}

$$X = A^{-1}B$$

\begin{equation}
    A = 
    \begin{bmatrix}
    2 & -5\\
    3 & 6\\
    \end{bmatrix}
    \qquad    B = 
    \begin{bmatrix}
    11\\
    3\\
    \end{bmatrix}
    \qquad    X = 
    \begin{bmatrix}
    x\\
    y\\
    \end{bmatrix}
\end{equation}


```python
A = np.array([[2, -5],
              [3, 6]])
B = np.array([[11],
              [3]])
print(f'Matriz A\n {A}\n')
print(f'Matriz B\n {B}')
```

    Matriz A
     [[ 2 -5]
     [ 3  6]]
    
    Matriz B
     [[11]
     [ 3]]



```python
# Matriz inversa de A
A_inv = np.linalg.inv(A)
A_inv
```




    array([[ 0.22222222,  0.18518519],
           [-0.11111111,  0.07407407]])




```python
#Resolução
X = A_inv @ B
print(f'x={float(X[0]):.2f} e y={float(X[1]):.2f}')
```

    x=3.00 e y=-1.00


# Exercício 4

## Matriz Insumo-Produto

**Demanda intermediária**

|Vendas/Despesas | Setor 1 | Setor 2 | Subtotal|
|---|---|---|---|
|Setor 1| 20 | 90 | 110|
|Setor 2| 40 | 180 | 220 |
|Subtotal| 60 | 270 | 330|

Dado que os valores brutos da produção dos setores 1 e 2 são, respectivamente, 200 e 600, e que as demandas finais são 90 e 380, encontre os multiplicadores setoriais.


## Referências

- Chen (2018). *Pandas for everyone: python data analysis* Addison-Wesley Professional.
- Marcondes (2018). *Matemática com Python*. São Paulo: Novatec.
- Menezes (2019). *Introdução à programação com Python*. 3 ed. São Paulo: Novatec.
- http://cs231n.github.io/python-numpy-tutorial/
- https://www.oreilly.com/library/view/machine-learning-with/9781491989371/ch01.html
- http://www.ie.ufrj.br/intranet/ie/userintranet/hpp/arquivos/matriz_insumo_pruduto_resumo.pdf

