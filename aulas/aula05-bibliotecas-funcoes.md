![ds-py-capa.png](attachment:ds-py-capa.png)


# Aula 05 - Módulos, pacotes e funções


## Objetivo
Apresentar noções sobre modularização, empacotamento e funções no `Python`.


## Conteúdo
1. [Bibliotecas](#bibliotecas)
2. [Funções: conceitos iniciais](#funcoes)
3. [Docstrings](#docstrings)
4. [Parâmetros opcionais](#parametros)
5. [Variáveis locais e globais](#variaveis)
6. [Módulos e pacotes](#modulos)
7. [Lambda - funções anônimas](#lambda)
8. [Classes](#classes)
9. [Herança e Polimorfismo](#heranca)
10. [Exercícios](#exercicios)

- compreensões de lista

<a name="bibliotecas"></a>
# Bibliotecas

## Instalação de pacotes

- No terminal: `pip3 install jupyter`.
- No Pycharm: preferences >> project >> interpreter >> + (install).
- Dica: no Pycharm, inseri no projeto um arquivo de texto `requirements` com todos os pacotes necessários.

## Importando bibliotecas

- Global: `import math`
- Alias (Apelido): `import math as m`
- Importação seletiva: `from math import sqrt`
- Importação não seletiva: `from math import *`


```python
import math

x=[100, 25, 81]

for i in x:
    print(math.sqrt(i))
```

    10.0
    5.0
    9.0



```python
import math as m

x=[100, 25, 81]

for i in x:
    print(m.sqrt(i))
```

    10.0
    5.0
    9.0



```python
from math import sqrt

x=[100, 25, 81]

for i in x:
    print(sqrt(i))
```

    10.0
    5.0
    9.0



```python
import emoji

# https://www.webfx.com/tools/emoji-cheat-sheet/
 
print('Aprender Python me deixa', 
      emoji.emojize(':smile:', use_aliases = True)*10)
```

    Aprender Python me deixa 😄😄😄😄😄😄😄😄😄😄



```python
print('Frutas que eu gosto:', emoji.emojize(':apple: :pear: :strawberry:', use_aliases = True))
```

    Frutas que eu gosto: 🍎 🍐 🍓


<a name="funcoes"></a>
# Funções

- Já vimos o uso de várias funções no `Python`, como `print`, `input`, `float`, `len` etc.
- E se quisermos criar nossa própria função, como proceder?


```python
# Função sem parâmetros
def linha():
    print('-' * 50)
```


```python
linha()
print("Aula de Python....")
linha()
```

    --------------------------------------------------
    Aula de Python....
    --------------------------------------------------



```python
# Função com parâmetros
def linha(x):
    print('-' * x)
```


```python
print("Aula de Python....")
linha(10)
```

    Aula de Python....
    ----------



```python
# Função com parâmetros defindo por DEFAULT
def linha(x=25):
    print('-' * x)
```


```python
print("Aula de Python....")
linha()
```

    Aula de Python....
    -------------------------



```python
def soma(x,y):
    print(x+y)
```


```python
a = soma(1,2)
a+3
```

    3



    -----------------------------------------------------------------------

    TypeError                             Traceback (most recent call last)

    <ipython-input-20-7719a112ceee> in <module>
          1 a = soma(1,2)
    ----> 2 a+3
    

    TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'



```python
def soma(x,y):
    return x+y

soma(1,2)+3
```




    6




```python
# Desempacotar (*): vários parâmetros
# imagine que o usuário queira passar vários números

def soma(* x):
    total = 0
    for i in x:
        total +=i
    return total     
```


```python
soma(1, 2, 3, 4)
```




    10




```python
# Somatório de listas
# imagine que o usuário queira passar vários números
def soma(x):
    total = 0
    for i in x:
        total +=i
    return total     

x = [1, 2, 3, 4]
soma(x)
```




    10



## Exemplo 01

Fazer uma função para cálculo da média aritmética de dois números


```python
def média(x,y):
    # RETURN assume um papel importante para retornar o valor de uma instrução DEF
    return (x+y)/2 
```


```python
média(10, 20)
```




    15.0



## Exemplo 02

Faça uma função para identificar se um número é par ou ímpar.


```python
def ParOuImpar(x):
    if (x % 2) == 0:
        return "par"
    else:
        return "ímpar"
```


```python
ParOuImpar(3)
```




    'ímpar'



## Exemplo 03

Função para cálculo de uma média em uma lista de valores.


```python
def avg(x):
    total = 0
    for i in x:
        total += i
    return print('Média =', total/len(x))
```


```python
x = [1, 3, 10, 20]
avg(x)
```

    Média = 8.5


## Exemplo 04

- Função para cálculo de um número fatorial. Por exemplo, o fatorial de 5 é:

$$5! = 5 \times 4 \times 3 \times 2 \times 1 = 120$$


```python
def fatorial(x):
    if x<0: 
        print('Erro: Número deve ser inteiro não negativo!')
    else:
        prod = 1
        while x>1:
            prod *= x
            x -= 1
        return prod
```


```python
fatorial(-1)
```

    Erro: Número deve ser inteiro não negativo!



```python
fatorial(5)
```




    120



## Funções recursivas

- Uma função pode chamar a si mesma
- Vamos refazer o exemplo do fatorial, de forma recursiva


```python
def fatorial(x):
    if x<0:
        print('Erro: Número deve ser inteiro não negativo!')
    elif x==0 or x==1:
        return 1
    else:
        return x * fatorial(x - 1)
```


```python
fatorial(5)
```




    120



<a name="docstrings"></a>
# Docstrings

- Documentação de strings do Python (ou docstrings): usada para descrever módulos, funções, classes e métodos do Python.
- Uma docstring é simplesmente uma string de várias linhas, inseridas logo abaixo do `DEF`.
- Como pedir ajuda da função `avg` criada anteriormente? Qual é o resultado?


```python
help(avg)
```

    Help on function avg in module __main__:
    
    avg(x)
    


## Inserindo Docstring em uma função


```python
def avg(x):
    """
    Retorna uma string com a média aritmética
    
    param x: objeto lista, tuplas.
    return: valor numérico
    """
    total = 0
    for i in x:
        total += i
        return total/len(x)
```


```python
help(avg)
```

    Help on function avg in module __main__:
    
    avg(x)
        Retorna uma string com a média aritmética
        
        param x: objeto lista, tuplas.
        return: valor numérico
    


<a name="parametros"></a>
# Parâmetros opcionais

`def avg(x=[0,0,0], imprimir=True):`

- Nesse caso `x` e `imprimir` estão como opcionais
- Com isso a função funcionaria sem nada: `avg()`
- A opção `imprimir` é uma booleana para definir se o retorno é numérico ou textual



```python
def avg(x=[0,0,0], imprimir=True):
    """
    Retorna uma string com a média aritmética
    
    param x: objeto lista, tuplas.
    imprimir por default é True, i.e., retorna string com o valor da média. Se imprimir for False, retorna um valor numérico.
    """
    total = 0
    for i in x:
        total += i
    if imprimir is True:
        return print(f'Média = {total/len(x):.2f}')
    else:
        return total/len(x)
```


```python
x = (10, 12, 15)
avg(x, imprimir=False)
```




    12.333333333333334



<a name="variaveis"></a>
# Variáveis locais e globais

- Definição de variáveis dentro (local) e fora (global) do escopo de uma função.


```python
def teste():
    print(f'DEF: A variável é global {n}')

n = 10    
print(f'OUT: A variável é global {n}')
teste()
```

    OUT: A variável é global 10
    DEF: A variável é global 10



```python
def teste():
    xs = 2
    print(f'DEF: A variável é global {n}')
    print(f'DEF: A variável é local {xs}')

n = 10    
print(f'OUT: A variável é global {n}')
print(f'OUT: A variável é local {xs}')
teste()
```

    OUT: A variável é global 10



    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-99-437c54231650> in <module>
          6 n = 10
          7 print(f'OUT: A variável é global {n}')
    ----> 8 print(f'OUT: A variável é local {xs}')
          9 teste()


    NameError: name 'xs' is not defined


<a name="except"></a>
# Validação e exceção

- Funções são muito úteis para validação de dados de entrada.
- E se na função fatorial o usuário digitasse um caracter? Ou um número real?

**Exemplo**: Validação de um número entre 0 e 10.


```python
def validar(x, minimo=0, maximo=10):
    while True:
        if  minimo <= x <= maximo:
            return x
            break
        else:
            print()
            x = int(input(f"Valor inválido. Digite um número entre {minimo} e {maximo}: "))
```


```python
validar(-1)
```

    
    Valor inválido. Digite um número entre 0 e 10: 4





    4



## Exceções

- `try` = testar erros é um bloco de códigos 

- `except` = tratamento em relação aos erros

- `finally` = execução de um código, independetemente dos resultados do `try` e `except`


```python
x = int('ABC')
# Testar outros erros, como: '2' + 2, 1/0
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-12-90e8fff33f66> in <module>
    ----> 1 x = int('ABC')
    

    ValueError: invalid literal for int() with base 10: 'ABC'


### Exemplo:

- Faça um programa que para ler um número inteiro, controlando para possíveis erros de entrada. 
- Enquanto o usuário não respeitar a regra, deve-se continuar a solicitação de um número.


```python
while True:
    try:
        x = input("Digite um número inteiro: ")
        y = int(x)
        print(y)
        break
    except ValueError:
        print(f"Erro! Entrada --{x}-- inválida. Tente novamente...")
```

    Digite um número inteiro: 10
    10


### Exemplo:

- Ao invés de fazer um programa, crie uma função que leia um número inteiro, controlando para possíveis erros de entrada. 
- Enquanto o usuário não respeitar a regra, deve-se continuar a solicitação de um número.


```python
def numInteiro(x):
    while True:
        try:
            y = int(x)
            return y
            break
        except ValueError:
            x = input(f"Erro! Entrada --{x}-- inválida. Digite novamente: ")
```


```python
numInteiro('A')
```

    Erro! Entrada --A-- inválida. Digite novamente: 10





    10



## Exemplo Número Fatorial

- Crie uma função para cálculo de um número fatorial, robusta a possíveis erros de entrada de dados. 

- Por exemplo, o fatorial de 5 é:

$$5! = 5 \times 4 \times 3 \times 2 \times 1 = 120$$

- Mas, se o usuário digitar uma letra ou um número negativo?


```python
def fatorial(x):
    try:
        x = int(x)
        if x < 0: 
            print(f'Erro: << {x} >> não é um número válido!')
        else:
            fat = 1
            for i in range(x, 1, -1):
                fat *= i
            print(f'O fatorial de {x} é {fat}')
    except ValueError:
        print(f'Erro: << {x} >> não é um número válido!')
```


```python
fatorial('a') # fatorial(-1), fatorial(5)
```

    Erro: << a >> não é um número válido!


 - Um mesmo bloco `try` pode conter vários `except`, bem como pode ter uma declaração `finally`.
 - O `finally` indica que um bloco será executado mesmo que aconteça uma exceção.
 - Dica: `Exception` cobre todos os erros comuns do `Python`.


```python
lanche = ('maçã', 'cookies', 'uvas', 'sanduíche')
for x in range(2):
    try: 
        i = int(input('Escolha dois lanches, usando um índice: '))
        print(lanche[i])
    except Exception as e:
        print(f"Entrada com algum erro: << {e} >>")
    finally:
        print(f'Escolha nº {x+1}')
```

    Escolha dois lanches, usando um índice: uva
    Entrada com algum erro: << invalid literal for int() with base 10: 'uva' >>
    Escolha nº 1
    Escolha dois lanches, usando um índice: 0
    maçã
    Escolha nº 2


<a name="lambda"></a>
# Lambda - funções anônimas

- Funções simples, sem nome, são chamadas de `lambda`.
- Geralmente usado quando o código da função é muito simples ou usado raramente.

- **Exemplo**: Função lambda que recebe um número e retorna o valor ao quadrado.


```python
a = lambda x: x**2
a(2), a(4), a(5)
```




    (4, 16, 25)



- **Exemplo**: Função lambda para a média geométrica de dois números.


```python
média = lambda x, y: round((x*y)**(0.5),2)
média(10,5)
```




    7.07



<a name="modulos"></a>
# Módulos e pacotes

## Módulos

- Após criarmos várias funções, os programas ficam muito grandes. 
- Por isso, que podemos armazenar nossas funções em um ou mais arquivos 'isolados'.
- Todo arquivo `.py` é um módulo, que pode ser importado usando `import`

**Vamos criar um dois módulos, um chamado `média.py` e outro `fatorial.py`**, em seguida vamos importá-los e usar as funções.

## Pacotes

- E se quisermos criar um pacote com várias funções, de forma mais organizada e estruturada?
- Característica: arquivo `__init__`

**Vamos criar um pacote chamado `utils` no `PyCharm`**: `new >> Python Package` 

<a name="classes"></a>
# Classes

- Uma classe é estrutura de dados que contém instância de atributos, instância de métodos e classes aninhadas.

- A classe é o que faz com que Python seja uma linguagem de programação orientada a objetos.

- As classes facilitam a **modularidade e abstração** de complexidade. O usuário de uma classe manipula objetos instanciados dessa classe somente com os métodos fornecidos por essa classe.

- Quando um objeto é criado, o **namespace** herda todos os nomes do namespace da classe onde o objeto está. O nome em um namespace de instância é chamado de **atributo** de instância.

- Um **método** é uma função criada na definição de uma classe. O primeiro argumento do método é sempre referenciado no início do processo. Por convenção, o **primeiro argumento do método** tem sempre o nome *self*. Portanto, os atributos de *self* são atributos de instância da classe.

- Por convenção, recomenda-se criar os nomes de classes usando a forma CamelCase.


```python
# Criando classe Pessoa

class Pessoa:
    # Função construtora
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
     
    # Método - alterar o atributo nome da própria classe
    def alterarNome(self, nome):
        self.nome = nome
     
    # Método - alterar o atributo idade da própria classe
    def alterarIdade(self, idade):
        self.idade = idade
     
    # Método - ver o atributo nome
    def verNome(self):
        return self.nome
    
    # Método - ver o atributo idade
    def verIdade(self):
        return self.idade
    
    # Método - celebração de aniversário
    def aniversarioIdade(self):
        return self.idade += 1


# Usando a classe/objeto pessoa
p1 = Pessoa("Maria da Silva", 16)
p2 = Pessoa("João Pereira", 38)

# Usando o método verNome
print(p1.verNome())

# Usando o método verIdade
print(p1.verIdade())

# Usando o método alterar Idade
p1.alterarIdade(40)
print(p1.verIdade())

print(p2.verNome())

```

    Maria da Silva
    16
    40
    João Pereira


<a name="heranca"></a>
# Herança e Polimorfismo

- Na Programação Orientada a Objetos o conceito de **Herança** é muito utilizado. Basicamente, dizemos que a herança ocorre quando uma **classe (filha) herda características e métodos de uma outra classe (pai)**, mas não impede de que a classe filha possua seus próprios métodos e atributos.

- **Polimorfismo** significa ter **algo único em vários lugares**. O polimorfismo é usado em classes distintas compartilhando funções em comum. Porque as classes derivadas são distintas, suas execuções podem diferir. Entretanto, as classes derivadas compartilham de uma relação comum, instâncias daquelas classes são usadas exatamente na mesma maneira.


```python
# Criando classe Pessoa

class Pessoa:
    # Função construtora
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
     
    # Método - alterar o atributo nome da própria classe
    def alterarNome(self, nome):
        self.nome = nome
     
    # Método - alterar o atributo idade da própria classe
    def alterarIdade(self, idade):
        self.idade = idade
     
    # Método - ver o atributo nome
    def verNome(self):
        return self.nome
    
    # Método - ver o atributo idade
    def verIdade(self):
        return self.idade

```


```python
# Por exemplo, podemos criar duas novas classes filhas que vão herdar os atributos da classe pai: Pessoa

# Criar classe filha : PessoaFisica passando Pessoa

class PessoaFisica(Pessoa):
    
    # Construtor: usamos o método super() para atribuir a herança da classe pai Pessoa
    def __init__(self, CPF, nome, idade, sexo):
        super().__init__(nome, idade)
        self.CPF = CPF
        self.sexo = sexo
    
    # Métodos próprios da classe PessoaFisica
    # Ver CPF
    def verCPF(self):
        return self.CPF
    
    # Alterar CPF
    def alterarCPF(self, CPF):
        self.CPF = CPF    
        
    # Método - alterar o atributo nome da própria classe
    def alterarSexo(self, sexo):
        self.sexo = sexo
```


```python
# Criar classe filha : PessoaJuridica

class PessoaJuridica(Pessoa):
    
    # Construtor: usamos o método super() para atribuir a herança da classe pai Pessoa
    def __init__(self, CNPJ, nome_fantasia, nome, idade):
        super().__init__(nome, idade)
        self.CNPJ = CNPJ
        self.nome_fantasia = nome_fantasia
     
    # Métodos próprios da classe PessoaJuridica
    # Ver CNPJ
    def verCNPJ(self):
        return self.CNPJ
    
    # Alterar CNPJ 
    def alterarCNPJ(self, CNPJ):
        self.CNPJ = CNPJ
        
    def verNomeFantasia(self, nome_fantasia):
        return self.nome_fantasia    

  
```


```python
x = PessoaJuridica(CNPJ='123', nome_fantasia='abc', 
                   nome='empresa abc', idade=20)
x.verIdade()
```




    20




```python
      
# Usando as classes        
p1 = Pessoa("José da Silva", 34)
print(p1)
print(p1.verNome())
print(p1.verIdade())

# Criar objeto usando a classe PessoaFisica
p2 = PessoaFisica(2345,"José da Silva", 34)

# Usar métodos herdados
print(p2.verNome())
print(p2.verIdade())

# Usar método específico
print(p2.verCPF())

# Alterar CPF
p2.alterarCPF(19234567)
print(p2.verCPF())


# Criar objeto usando a classe PessoaJuridica
p2 = PessoaJuridica(12340001,"Empresa XY", 25)

# Usar métodos herdados
print(p2.verNome())
print(p2.verIdade())

# Usar método específico
print(p2.verCNPJ())

# Alterar CPF
p2.alterarCNPJ(150000999)
print(p2.verCNPJ())
```

    <__main__.Pessoa object at 0x10e0429e8>
    José da Silva
    34
    José da Silva
    34
    2345
    19234567
    Empresa XY
    25
    12340001
    150000999


## Herança múltipla

Uma classe pode ser derivada de uma ou mais classes base.



```python
class A(object):
    def f():
        pass

class B(object):
    pass

    def g(): pass

class C(A, B):
    pass

# Conseqüentemente, a classe C herda atributos da classe de ambas as classes base.
```

## Métodos estáticos

Um método estático é uma atribuição a classe que não precisa do primeiro argumento para ser instanciado na classe. (Normalmente métodos precisam do primeiro argumento como self, para ser uma instância da classe).


```python
class Retângulo(object):
    @staticmethod
    def area(base, altura):
        return base*altura
    
q = Retângulo()
print(q.area(2,5))
```

    10


## Propriedades

Um property é um atributo de classe que promove o método de acesso e/ou método modificador. 
Você pode usar propriedades instanciando atributos e utilizando a notação de invocação dos métodos accessor e mutator. Novamente, o operador . (ponto) é utilizado para especificar o objeto.


```python

class Retângulo(object):
    
    def __init__(self, nome):
        self.nome = nome
    
    @staticmethod
    def calcularArea(base, altura):
        return base*altura
    
    @property
    def alerta(self):
        return self.nome
    
    
q = Retângulo("Meu exemplo") 
q.alerta
```




    'Meu exemplo'



<a name='exercicios'> </a>
# Exercícios

1- Faça uma função para calcular a média aritmética.

2- Faça uma função para calcular a média geométrica.

3- Faça uma função para calcular o fatorial de um número.

4- Repita a questão anterior em um módulo.

5- Empacote as três funções criadas em um pacote chamado `myStats`

## Referências

- Chen (2018). *Pandas for everyone: python data analysis* Addison-Wesley Professional.
- Marcondes (2018). *Matemática com Python*. São Paulo: Novatec.
- Menezes (2019). *Introdução à programação com Python*. 3 ed. São Paulo: Novatec.
