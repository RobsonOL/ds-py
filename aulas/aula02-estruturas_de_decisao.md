
![ds-py-capa.png](attachment:ds-py-capa.png)


# Aula 02 - Estruturas de decisão


## Objetivo
Apresentar regras de decisões e suas estruturas simples e aninhadas para modelar diferentes condições para um resultado.


## Conteúdo
1. [Operados relacionais e lógicos](#booleanas) 
2. [Operadores lógicos](#operadores1)
3. [if, else](#estruturas)
4. [Estruturas aninhadas](#aninhamento)
5. [elif (else if)](#elif)
6. [Exercícios](#exercicios)

 

<a name="booleanas"></a>

# Variáveis booleanas e operadores de comparação

As variáveis booleanas no Python assumem `True` ou `False`. Elas são muito usadas em testes de comparação e
regras de decisões.



```python
False
type(False)
```




    bool




```python
True
type(True)
```




    bool



## Operadores Relacionais

|Operador | Operação |
|:---:|:---|
|==|Identidade (igualdade)|
|is|Identidade (igualdade)|
|!=|Diferente|
|is not|Diferente|
|>|Maior que|
|<|Menor que|
|>=|Maior ou igual|
|<=|Menor ou igual|

Vejamos algums exemplos com comparações entre números:


```python
# Operador de comparação
20 > 15
```




    True




```python
# Operador de comparação
20 <= 15
```




    False




```python
# Operador de comparação
20 <= 20
```




    True




```python
# Operador de comparação
10 == 10
```




    True




```python
# Operador de comparação
10 is 10
```




    True




```python
# Operador de comparação
20 != "20"
```




    True




```python
# Operador de comparação
20 is not 20
```




    False




```python
# Operador de comparação
1 == True
```




    True




```python
# Operador de comparação
0 == False
```




    True



# Exemplo 01

Faça um programa para saber se um indivíduo é maior de idade.


```python
idade = int(input("Digite sua idade:"))
resposta = idade >= 18
print(resposta)
```

    True


<a name="operadores1"></a>
# Operadores lógicos

Os operadores de comparação mais usados em testes múltiplos:

|Operador | Operação |
|:---:|:---|
|and|Intersecção|
|or|União|
|not|Negação|
|in|é elemento do objeto?|



```python
not True is False
```




    True




```python
not False is True
```




    True



## Exemplo 02

a. Se um aluno tiver média acima de 7.0 e frequência de 75% ou mais, ele será aprovado.

b. Caso o aluno tenha duas ou mais reprovações ou CRA menor que 7.0, ele será desligado.


```python
nota = 8.3
frequencia = 0.61

reprovacoes = 1
cra = 6.4

aprovado = nota>=7 and frequencia>=0.75
print(aprovado)

desligado = reprovacoes>=2 or cra<7
print(desligado)
```

    False
    True


<a name="estruturas"></a>
# Estruturas de decisão 

Para a tomada de decisão em um algoritmo torna-se necessário a construção de *expressões lógicas condicionais*.

*Observação* 
- Trívia (identação): `Python` usa o recuo à direita para marcar o início e o fim de um bloco
- Outras linguagens usam `BEGIN` e `END` (como Pascal) e as famosas `{ }` (como C, Java e R).

## if

```
if <condição>:
    bloco verdadeiro
```

*Exemplo:* dado uma média final em uma disciplina, o que podemos decidir?


```python
nota = float(input('Informe sua média final: '))
if nota>=7:
    print('Aprovado')
if nota<7:
    print('Reprovado')
```

    Informe sua média final: 3.5
    Reprovado


## else 

Quando uma decisão lógica é simplesmente o resultado complementar de condições anteriores podemos usar a cláusula `else`.

```
if <condição>:
    bloco verdadeiro
else:
    bloco verdadeiro
```

## Exemplo 03

Escreva um programa para determinar se uma pessoa deve pagar ou não um imposto. Os impostos incidem em pessoas com rendimento mensal acima de R$ 1200, mas bolsistas e aposentados são isentos.


```python
renda = float(input('Informe sua renda mensal: '))
bolsista = bool(int(input('Digite 1 se for bolsista e 0 se c.c.: ')))
aposentado = bool(int(input('Digite 1 se for aposentado e 0 se c.c.: ')))

if renda>1200 and bolsista is False and aposentado is False:
    print('Você deverá pagar imposto!')
else:
    print('Você possui isenção!')


```

    Informe sua renda mensal: 3000
    Digite 1 se for bolsista e 0 se c.c.: 0
    Digite 1 se for aposentado e 0 se c.c.: 0
    Você deverá pagar imposto!


<a name="aninhamento"></a>
# Estruturas aninhadas

Às vezes pode ser necessário aninhar vários `if` para obter um comportamento/resultado desejado.


```
if <condição>:
    if <condição>:
        <resultado 1>
    else <condição>:
        <resultado 2>  
else:
    if <condição>:
        <resultado 3>
    else <condição>:
        <resultado 4>  
```



## Exemplo 04

Escreva um programa que calcule o preço a pagar pelo fornecimento de energia elétrica. Pergunte a quantidade de energia elétrica consumida (kWh) e o tipo de instalação: *R* para residências, *C* para comércios e *I* para indústrias. Considere os dados abaixo com o preço por tipo e faixa de consumo:

|Tipo|Faixa (kWh) | Preço|
|:---:|:---:|:---:|
Residencial| Até 500 | 0,40|
Residencial| Acima de 500 | 0,65|
Comercial| Até 1000 | 0,55|
Comercial| Acima 1000 | 0,60|
Industrial| Até 5000 | 0,55|
Industrial| Até 5000 | 0,60|




```python
consumo = 100
tipo = 'R'

if tipo == 'R':
    cliente = 'A sua residência'
    if consumo<=500:
        tarifa = 0.4
    else:
        tarifa = 0.65
if tipo == 'C':
    cliente = 'O seu comércio'
    if consumo<=1000:
        tarifa = 0.55
    else:
        tarifa = 0.60
if tipo == 'I':
    cliente = 'A sua indústria'
    if consumo<=5000:
        tarifa = 0.55
    else:
        tarifa = 0.60
```


```python
# Resultados
valor = consumo * tarifa

print(f'''
{cliente} teve um consumo de {consumo} kWh de energia neste mês. 
A tarifa para essa faixa de consumo é de R$ {tarifa:.2f}.
Dessa forma, a sua conta foi de R$ {valor:.2f}
''') 
```

    
    A sua residência teve um consumo de 100 kWh de energia neste mês. 
    A tarifa para essa faixa de consumo é de R$ 0.40.
    Dessa forma, a sua conta foi de R$ 40.00
    


<a name="elif"></a>
# elif (else if)

O `Pyhton` apresenta uma solução interessante para o caso dos múltiplos `if`, evitando a criação de outro nível de estrutua condicional.

```
if <condição>:
    <resultado 1>
elif <condição>:
    <resultado 2>
else:
    <resultado 3>    
```

## Exemplo 05

Faça um programa para calcular o IMC de uma pessoa e informar seu atual status nutricional.

O IMC é calculado por peso/(altura*altura) e pode trazer os seguintes resultados:

| Intervalo | Condição| 
|:------:| :--------:|
Menos do que 18,5 |	Abaixo do peso |
Entre 18,5 e 24,9 |	Peso normal |
Entre 25 e 29,9	| Sobrepeso |
Entre 30 e 34,9	| Obesidade grau 1 |
Entre 35 e 39,9	| Obesidade grau 2 |
Mais do que 40	| Obesidade grau 3 |


```python
# Solicitar os dados do paciente
nome = str(input("Informe o seu nome:"))

# Solicitar os dados do paciente
altura = float(input("Informe sua altura:"))

# Solicitar os dados do paciente
peso = float(input("Informe seu peso:"))

# Calcular o IMC 
imc = peso/(altura*altura)
```

    Informe o seu nome:João
    Informe sua altura:1.60
    Informe seu peso:120



```python
# Criar variável condição com estrutura/regras de decisão
# Já adiantamos alguns operadores lógicos ( >= , <=, > , and, etc)
if imc < 18.5:
    condicao = "abaixo do peso"
elif imc <= 24.9:
    condicao = "peso normal"
elif imc <= 29.9:
    condicao = "sobrepeso"
elif imc <= 34.9:
    condicao = "obesidade grau 1"    
elif imc <= 39.9:
    condicao = "obesidade grau 2"  
else:
    condicao = "obesidade grau 3" 
```


```python
# Reportar resultado
print("%s registra peso de %.1f kg e altura de %.2f metro(s). Seu IMC é %.2f. "
      "Portanto, sua condição é %s" %(nome, peso, altura, imc, condicao) )      
```

<a name="exercicios"> </a>
# Exercícios

1- Verifique se a entrada digitada é numérica, texto, alfanumérica e se possui algum caracter especial.

2- Faça um Programa que verifique se uma letra digitada é "F" ou "M". Conforme a letra escrever: F - Feminino, M - Masculino. Para quaisquer outra string, inserir: Sexo Inválido.

3- Faça um programa que pergunte o preço de três produtos e informe qual produto você deve comprar, sabendo que a decisão é sempre pelo mais barato.

4- Faça um Programa que peça as 4 notas bimestrais, mostre a média e apresente as seguintes mensagens:

 - "Aprovado", se a média alcançada for maior ou igual a sete;
 - "Reprovado", se a média for menor do que sete;
 - "Final", se a média for entre 4 e 6.9;
 - "Aprovado com Distinção", se a média for igual a dez.

Resolva esse programa usando: a) apenas `if`, b) `else if`, c) estruturas aninhadas.

5- Tendo como dados de entrada a altura e o peso de uma pessoa, construa um algoritmo que calcule um intervalo de peso ideal e informe se ela está dentro, acima ou abaixo do peso.

Dica: use a fórmula do IMC, considerando o estrato *eutrófico* como referência para as estimativas.

6- Faça um programa que calcule as raízes de uma equação do segundo grau, na forma ax2 + bx + c. O programa deverá pedir os valores de a, b e c e fazer as consistências, informando ao usuário nas seguintes situações:

    a.	Se o usuário informar o valor de A igual a zero, a equação não é do segundo grau e o programa não deve fazer pedir os demais valores, sendo encerrado;

    b.	Se o delta calculado for negativo, a equação não possui raizes reais. Informe ao usuário e encerre o programa;

    c.	Se o delta calculado for igual a zero a equação possui apenas uma raiz real; informe-a ao usuário;

    d.	Se o delta for positivo, a equação possui duas raiz reais; informe-as ao usuário;
    

7- Faça um Programa para um caixa eletrônico. O programa deverá perguntar ao usuário a valor do saque e depois informar quantas notas de cada valor serão fornecidas. As notas disponíveis serão as de 1, 5, 10, 50 e 100 reais. O valor mínimo é de 10 reais e o máximo de 600 reais. O programa não deve se preocupar com a quantidade de notas existentes na máquina.

 - Exemplo 1: Para sacar a quantia de 256 reais, o programa fornece duas notas de 100, uma nota de 50, uma nota de 5 e uma nota de 1;

 - Exemplo 2: Para sacar a quantia de 399 reais, o programa fornece três notas de 100, uma nota de 50, quatro notas de 10, uma nota de 5 e quatro notas de 1.

## Referências

- Chen (2018). *Pandas for everyone: python data analysis* Addison-Wesley Professional.
- Marcondes (2018). *Matemática com Python*. São Paulo: Novatec.
- Menezes (2019). *Introdução à programação com Python*. 3 ed. São Paulo: Novatec.
