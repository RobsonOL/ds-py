
![ds-py-capa.png](attachment:ds-py-capa.png)


# Aula 01 - Preparando o ambiente


## Objetivo
Instalação dos softwares e apresentação geral da linguagem e da IDE Pycharm.


## Conteúdo
1. [Instalação de um interpretador da linguagem `Python`](#instalacao) 
2. [Utilização do interpretador](#usando)
3. [IDE PyCharm e suas vantagens](#pycharm)
4. [Algumas recomendações ao escrever seus scripts em Python](#recomendacoes)
5. [Variáveis e atribuição](#variaveis)
6. [Tipos primitivos](#tipos)
7. [Entrada de dados](#input)
8. [A função `print` e suas opções: F-Strings, format e %](#print)
7. [Exercícios](#exercicios)

 

<a name="instalacao"></a>
# Instalação do Python 3.7 ou +

O [Python](https://www.python.org/about/) é compatível com Windows, Linux e o Mac OS. Ele já é nativo nos dois últimos (v2.7)


## Mac
<https://www.python.org/downloads/mac-osx/>

Via terminal

```
which python
xcode-select --install
sudo easy_install pip
sudo pip install --upgrade pip
ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"
brew doctor
brew install python3
python3 --version
python3
```


## Linux
<https://www.python.org/downloads/source/>

```
python --version
sudo apt-get update
sudo apt-get install -y python3.7 idle-python3.7
sudo apt-get install python-pip
```


## Windows
<https://www.python.org/downloads/windows/>



<a name="usando"></a>
# Usando o interpretador Python 

## Opção 1

No terminal digitar:

```python3```




```python
# Meu primeiro programa
print('Hello, World!')

```

    Hello, World!



```python
import this
```

    The Zen of Python, by Tim Peters
    
    Beautiful is better than ugly.
    Explicit is better than implicit.
    Simple is better than complex.
    Complex is better than complicated.
    Flat is better than nested.
    Sparse is better than dense.
    Readability counts.
    Special cases aren't special enough to break the rules.
    Although practicality beats purity.
    Errors should never pass silently.
    Unless explicitly silenced.
    In the face of ambiguity, refuse the temptation to guess.
    There should be one-- and preferably only one --obvious way to do it.
    Although that way may not be obvious at first unless you're Dutch.
    Now is better than never.
    Although never is often better than *right* now.
    If the implementation is hard to explain, it's a bad idea.
    If the implementation is easy to explain, it may be a good idea.
    Namespaces are one honking great idea -- let's do more of those!


# Usando o interpretador Python

## Opção 2: versão com uma interface mais amigável

No terminal digitar:

- Mac OS X: `idle-python3.7 &`
 
- Linux:  `IDLE3.7 &`


```python
# Podemos usar o interpretador como calculadora
1 + 1
```




    2



<a name="pycharm"></a>
# Instalação do PyCharm

Usando o e-mail institucional da UFPB (exemplo: nome@ccsa.ufpb.br), temos acesso a versão *Professional*

<https://www.jetbrains.com/pycharm/download/>

*Por que usar o Pycharm? Só existe esse IDE?*

## Mac
<https://download.jetbrains.com/python/pycharm-community-2019.2.dmg> 

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null ; brew install caskroom/cask/brew-cask 2> /dev/null
brew cask install pycharm
```

## Windows
<https://download.jetbrains.com/python/pycharm-community-2019.2.exe>

## Linux
<https://download.jetbrains.com/python/pycharm-community-2019.2.tar.gz>

## Stack Overflow

Ambiente para auxiliar o cientista de dados!

<https://stackoverflow.com/>

<a name="recomendacoes"> </a>
# Algumas recomendações ao escrever seus scripts em Python

1. O `Python` é case sensitive: `A is not a` ou `print is not Print`.
2. Aspas (simples ou duplas) não devem ser esquecidas.
3. Parênteses não são opcionais.
4. Espaços são extremamente importantes: a linguagem é baseada em *identação* para definir o escopo/bloco de tarefas.
5. Uso de comentários torna o script mais amigável e replicável.
6. A função `help` deve ser sempre consultada para tirar dúvidas: `help(print)`

## Comentários

Comentários curtos no Python devem ser precedidos de #. Vejamos um exemplo:


```python
#%%

# Imprimir o resultado
# Calcular média
# Ler base de dados

#%% md
```

Já comentários em múltiplas linhas devem se situar entre: """ comentários """. Vejamos um exemplo:


```python
"""
Este programa busca identificar a presença de 
dados aberrantes.

-----------------
fulano@lema.ufpb.br
Universidade Federal da Paraíba

"""
```

# Operadores e operações matemáticas básicas

|Operador|Operação|Exemplo|
|:---:|:---:|:---:|
|+ | Adição | `1 + 1 = 2`|
|- | Subtração | `1 - 2 = -1`|
|* | Multiplicação | `1 * 3 = 3`|
|/ | Divisão (resultado fracionário) | `3 / 2 = 1.5`|
|// | Divisão (resultado inteiros) | `3 // 2 = 1`|
|% | Resto da divisão | `3 % 2 = 1`|
|** | Exponenciação | `4 ** 2 = 16`|

## Ordem de precedência das operações matemáticas

1. Exponenciação (**)
2. Multiplicação (*), divisão (/ e //) e resto (%)
3. Adição (+) e subtração (-)

*Observação*: os parênteses podem ser usados para alterar a ordem de execução de uma operação.

A expressão $$10 + 15 * 3 / 2$$ é igual a quanto no `Python`?

E se fosse $(10 + 15) * 3 / 2$, o resultado seria o mesmo?

<a name="variaveis"> </a>
# Variáveis e atribuição

- Em matemática, o que significa uma variável? E em programação?

- 1) Representa uma incógnita numa equação; 2) Armazena valores na memória do computador onde se armazena dados.
- Para armazenar algo na memória usaremos o sinal de igualdade (=). *Operação de atribuição*


```python
# Programa com variáveis
x = 12
y = 5
print(x + y)
```


```python
x = [1, 3, 5]
print("Meu primeiro vetor é composto por:", x)
```

    Meu primeiro vetor é composto por: [1, 3, 5]


- O `print` é uma função que realiza a impressão dos resultados. Toda função em `Python` requer o uso de ( ).

# A função `print` básica

No `Python 3` essa função passou por mudanças significativas: <https://docs.python.org/3/whatsnew/3.0.html>

```
print("Olá, mundo!")
print("15")
print(15)
print("Eu tirei com nota ", 9)
print("Eu tenho", 25, "anos.")
```



```python
# Quebrar linhas \n e tabulações \t

print("\n \n Meu nome é: \n \n \n \t \t \t Enéas!")
```

    
     
     Meu nome é: 
     
     
     	 	 	 Enéas!



```python
# Buscar ajuda
help(print)
```

    Meu nome é: 
     Enéas!
    Help on built-in function print in module builtins:
    
    print(...)
        print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
        
        Prints the values to a stream, or to sys.stdout by default.
        Optional keyword arguments:
        file:  a file-like object (stream); defaults to the current sys.stdout.
        sep:   string inserted between values, default a space.
        end:   string appended after the last value, default a newline.
        flush: whether to forcibly flush the stream.
    


<a name="input"> </a>
# Entrada de dados

## O caso da função `input`

O melhor da programação é escrever soluções de um problema e fazer sua generalização/repetição. A função `input` é usada para solicitar dados ao usuário.

- Exemplo: anos de estudo de um indivíduo



```python

nome=input("Digite seu nome: ")
idade_atual=input("Digite sua idade atual: ")
idade_escola=input("Digite sua idade de quando iniciou os estudos: ")

print(f"Nome: {nome}, Idade atual: {idade_atual}")
```

    Digite seu nome: Ana
    Digite sua idade atual: 21
    Digite sua idade de quando iniciou os estudos: 5
    Nome: Ana, Idade atual: 21



```python
- A entrada (`input`) recebida sempre será da classe `STRING`. E se precisarmos executar algum cálculo?
```


```python
anos_estudo = idade_atual - idade_escola
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-98-84c1b2a13381> in <module>
    ----> 1 anos_estudo = idade_atual - idade_escola
    

    TypeError: unsupported operand type(s) for -: 'str' and 'str'



```python
anos_estudo = int(idade_atual) - int(idade_escola)

print(f'Tempo de estudo formal até agora: {anos_estudo} anos')
```

    Tempo de estudo formal até agora: 16 anos


## Nomes de variáveis

|Nome|Válido|Comentário|
|---|---|---|
|Preço|True|O `Python 3` permite a utilização de acentuação (padrão UTF-8)|
|Preço da água|False|Não pode conter espaços em branco|
|salário_médio|True|A sublinha pode ser usado|
|salário.médio|False|O ponto não pode ser usado|
|p1|True|Variáveis podem conter números, desde que não seja no início|
|1a|False|Variáveis não podem conter números no início|

## Operadores de atribuição

Suponha sempre que inicialmente $x=2$, em cada operação na tabela abaixo. 

|Operação|Resultado|Equivalência|
|---|---|---|
|`x += 1`|3|`x = x + 1`|
|`x -= 1`|1|`x = x - 1`|
|`x *= 2`|4|`x = x * 2`|
|`x **= 3`|8|`x = x ** 3`|
|`x /= 3`|0.67|`x = x ** 3`|
|`x //= 3`|0|`x = x // 3`|
|`x %= 3`|2|`x = x % 3`|

## Exercício

Faça um programa que calcule o novo preço de um produto após um aumento de 20% nos tributos.

<a name="tipos"> </a>
# Tipos primitivos de objetos do Python

O Python tem quatro tipos de variáveis primitivas:

| Tipo | Descrição | Exemplo|
|:------:| :--------:| :------:
|int| inteiro| 12, -4, 0, 1112|
|float | reais (ponto flutuante) | 2.5, 0.06, 10.0
|bool| booleano (lógico)| True, False
|str | string (texto)| 'Olá', '7.5', "Caixa d'água"


Nos exemplos a seguir, uma sugestão é usar a função `input` para receber os valores das variáveis. Para mais informações `help(input)`.




```python
# Exemplo 1.
# Necessário explicitar o tipo da entrada!
# Por padrão, input é tipificado como string
# help(float)
# help(input)

#n1 = float(input("Por favor, digite um número: "))
#n2 = float(input("Por favor, digite um número: "))

n1 = 12
n2 = 1.5

s = n1 + n2

print('A soma entre', n1, 'e', n2, 'vale', s)

```

    A soma entre 12 e 1.5 vale 13.5


<a name="print"> </a>
# A função `print` e suas opções: F-Strings, format e %

No `Python 3.6 ou +` é possível usar uma sintaxe bem poderosa para impressão de resultados de um programa, como `F-string` e `%`

- Para mais efetividade de seu uso, faz-se necessário conhecer os marcadores

|Marcador|Tipo|
|:---:|---|
|%d|Números inteiros|
|%f|Números reais|
|%s|Strings|

- Atualmente, o método mais flexível é o `F-string`, vejam os exemplos a seguir.
- Podemos usar `<, >, ^` para alinhar à esquerda, direira ou centro: `f"{R$ x:>10.2f}"`




```python
nome = "João"  # string (str, %s)
peso = 80      # inteiro (int, %d)
altura = 1.729 # real (float, %f)

# Ver o tipo de cada objeto
tipo_nome = type(nome)
tipo_peso = type(peso)
tipo_altura = type(altura)

print(tipo_nome, tipo_peso, tipo_altura)

```

    <class 'str'> <class 'int'> <class 'float'>



```python
#Opção 1: menos operacional
print("Alô, ", nome, "! O seu peso é ", peso, " kg e sua altura é ", altura, " m.", sep="")
```


```python
#Opção 2: mais flexível
print("Alô, {}! O seu peso é {} kg e sua altura é {:.2f} m.".format(nome, peso, altura))
```

    Alô, João! O seu peso é 80 kg e sua altura é 1.73 m.



```python
#Opção 3: ainda mais flexível
print("Alô, %s! O seu peso é %d kg e sua altura é %.2f m."%(nome, peso, altura))
```

    Alô, João! O seu peso é 80 kg e sua altura é 1.73 m.



```python
#Opção 4: a mais nova das opções
print(f"Alô, {nome}! O seu peso é {peso} kg e sua altura é {altura:.2f} m.")
```

    Alô, João! O seu peso é 80 kg e sua altura é 1.73 m.


<a name="exercicios"> </a>
## Exercícios práticos

- Vamos começar a pensar como um(a) programador(a)? 

- Cada equipe terá um tempo determinado para resolver os problemas e todos terão que apresentar os resultados. 


1- Fazer um código para checar se um número digitado é positivo, negativo ou zero. 

2- Fazer um código para checar se um número digitado é par ou ímpar. 

3- Faça um programa que pergunte o quanto você ganha por hora e o total de horas trabalhadas por mês. Calcule e mostre o total do seu salário no mês.

4- Faça um programa para loja de tintas. O programa deverá pedir o tamanho em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros, que custam 80,00 cada. 

- Informe ao usuário a quantidade de latas de tinta a serem compradas e o preço total.

5- Faça um programa para uma loja de tintas. O programa deverá pedir o tamanho em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros, que custam 80,00. Informe ao usuário a quantidades de latas de tinta a serem compradas e o preço total.

6- Faça outro programa para loja de tintas. O programa deverá pedir o tamanho em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 6 metros quadrados e que a tinta é vendida em latas de 18 litros, que custam 80,00 ou em galões de 4 litros, que custam 25,00. Informe ao usuário as quantidades de tinta a serem compradas e os respectivos preços em 3 situações:
 - comprar apenas latas de 18 litros;
 - comprar apenas galões de 4 litros;
 - mistrurar latas e galões. 
 

7- Faça um programa de matemática financeira, para simular o valor de uma prestação do veículo,  com as seguintes entradas: a) valor do produto; b) Entrada; c) Número de prestações a.m. para pagamento do saldo; d) taxa de juros a.m. Para sua realização, assuma um regime de juros composto.
 
8- Faça um Programa que peça as 4 notas bimestrais, mostre a média e o status do aluno (aprovado, reprovado, final).

9- Tendo como dados de entrada a altura e o peso de uma pessoa, construa um algoritmo que calcule seu peso ideal (usando a fórmula do IMC) e informe se ela está dentro, acima ou abaixo do peso.

10- Faça um Programa que pergunte quanto você ganha por hora e o número de horas trabalhadas no mês. Calcule e mostre o total do seu salário no referido mês, sabendo-se que são descontados 11% para o Imposto de Renda, 8% para o INSS e 5% para o sindicato, faça um programa que nos dê: a) salário bruto. b) valor pago de INSS. c) contribuição do sindicato. d) salário líquido.

## Referências

- Chen (2018). *Pandas for everyone: python data analysis* Addison-Wesley Professional.
- Marcondes (2018). *Matemática com Python*. São Paulo: Novatec.
- Menezes (2019). *Introdução à programação com Python*. 3 ed. São Paulo: Novatec.
