![ds-py-capa.png](attachment:ds-py-capa.png)


# Aula 04 - Strings


## Objetivo
Apresentar operações com strings no `Pyhton`.


## Conteúdo
1. [Variável string](#variavel) 
2. [Operações com Strings](#operacoes)
3. [Verificação parcial de strings](#verificacao)
4. [Contagem, pesquisa e posicionamento](#contagem)
5. [Quebra de string e substituição de letras e palavras](#replace)
6. [Validação por tipo de conteúdo](#validacao)
7. [Formatação de string](#formatacao)
8. [Exercícios](#exercicios)


<a name="variavel"></a>
# Variável string

- Armazena cadeia de caracteres como uma sequência de blocos
- Cada caracter ocupa uma posição: vejam o exemplo abaixo

|Texto |P|A|R|A|Í|B|A| |8|3|
|-|-|-|-|-|-|-|-|-|-|-|
|Índice|0|1|2|3|4|5|6|7|8|9|

- Podemos acessar strings como listas, informando o índice entre `[ ]`.



```python
x = "PARAÍBA 83"
x
```




    'PARAÍBA 83'




```python
x[0]
```




    'P'



## Concatenação


```python
x = 'ABC'
x + 'D'
```




    'ABCD'




```python
x + 'D' * 3
```




    'ABCDDD'



## Fatiamento



```python
x = 'ABCDEFG'
x[0:3]
```




    'ABC'




```python
x = 'ABCDEFG'
x[0:3]
```




    'ABC'




```python
x = 'ABCDEFG'
x[-1]
```




    'G'




```python
# Este tipo de concantenção só funciona com strings
x + 10
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-23-dd76fc2ffa23> in <module>
          1 # Este tipo de concantenção só funciona com strings
    ----> 2 x + 10
    

    TypeError: can only concatenate str (not "int") to str


<a name='operacoes'> </a>
# Operações com Strings

- Strings são imutáveis.
- Alterações podem ser feitas por transformação em lista ou usando o método `replace`.


```python
x = "Lema Ufpb"
print(x[0])
```

    L



```python
x[0] = 'l'
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-79-255fc1e4853b> in <module>
    ----> 1 x[0] = 'l'
    

    TypeError: 'str' object does not support item assignment


## Para alterar uma string precisamos transformá-la em uma lista

- Modo 1 - Lista
- Modo 2 - `replace` (será mostrado mais a frente!)


```python
x = list('Lema Ufpb')
print(x, '\n')
```

    ['L', 'e', 'm', 'a', ' ', 'U', 'f', 'p', 'b'] 
    



```python
x[0] = 'l'
print(x)
```

    ['l', 'e', 'm', 'a', ' ', 'U', 'f', 'p', 'b']



```python
# Transformando em string novamente
x = ''.join(x)
x
```




    'lema Ufpb'



<a name="verificacao"></a>
# Verificação parcial de strings

- Para verificar o conteúdo de uma string de forma parcial podemos usar as funções `startswith` ou `endswith`
- De forma mais simples podemos fazer a comparação usando `in` ou `not in`



```python
x = 'lema ufpb'
x.startswith('lema')
```




    True




```python
x.startswith('LEMA')
```




    False




```python
# para resolver problema Case sensitive
x.upper().startswith('LEMA') #x.startswith('lema'.lower())
```




    True




```python
x.endswith('ufpb')
```




    True



## Usando IN ou NOT IN



```python
# outra forma de verificar

'lema' in x

```




    True




```python
'ufpb' in x.lower()
```




    True




```python
#Usando NOT IN para comparação
'federal' not in x
```




    True



<a name="contagem"></a>
# Contagem, pesquisa e posicionamento

## Contagem

- Pode ser do interesse fazer a contagem de letras ou palavras em uma string


```python
x = 'Paraíba, Brasil-PB. A Paraíba tem 223 municípios.'

#contagem de letras ou palavras
x.lower().count('P'.lower())
```




    4




```python
x.lower().count('paraíba')
```




    2



## Pesquisa em Strings

- Para isso, podemos usar a função `find` e suas derivadas, como `rfind`


```python
x = 'Paraíba, Brasil-PB. A Paraíba tem 223 municípios.'
x.find('Paraíba') # obtém a primeira posição da ocorrência
```




    0




```python
x.find('PE') # Item não encontrado = -1
```




    -1




```python
x.rfind('Paraíba') # obtém a primeira posição da ocorrência a direita
```




    22



##  Exemplo

Pesquisar todas as ocorrências da palavra Paraíba, na seguinte string

- **Paraíba, Brasil-PB. A Paraíba tem 223 municípios.**

Dica: `find` aceita START e STOP como opção.


```python
x = 'Paraíba, Brasil-PB. A Paraíba tem 223 municípios.'
p=0
while (p > -1):
    p = x.find('Paraíba', p)
    if p >= 0:
        print(f'Posição: {p+1}/{len(x)}')
        p += 1
```

    Posição: 1/49
    Posição: 23/49


##  Posicionamento de uma string

- As funções `center`, `ljust` e `rjust` podem ser utilizadas para uma melhor definição de posição de um texto.


```python
# posicionamento
x = 'lema'
'<' + x.center(10) + '>'
```




    '<   lema   >'




```python
'<' + x.upper().center(10, "-") + '>'
```




    '<---LEMA--->'




```python
# posicionar string a esquerda com adição de espaços ou caracteres
x.ljust(20, ".")
```




    'lema................'




```python
# posicionar string a direita com adição de espaços ou caracteres
x.rjust(20, '.')
```




    '................lema'



<a name="replace"></a>
# Quebra de string e substituição de letras e palavras

## Quebra/separação de uma string

- A função `split` pode ser muito útil para quebrar uma string para compor uma lista


```python
x = 'Arroz, Batata, Pão'
x.split(' ')

```




    ['Arroz,', 'Batata,', 'Pão']




```python
x = 'Arroz, Batata, Pão'
x.split(', ')
```




    ['Arroz', 'Batata', 'Pão']



## Quebra de uma string por linhas \n

- Em alguns casos, podemos querer quebrar strings que estão separadas por linha (\n)


```python
x = 'Arroz\nBatata\nPão'
print(x)
```

    Arroz
    Batata
    Pão



```python
x.splitlines()
```




    ['Arroz', 'Batata', 'Pão']



## Substituição de texto em uma string

- O método `replace` pode ser usado para tal finalidade.
- Ele ainda possui a opção para definirmos a quantidade de substituições


```python
x = 'Um tigre, dois tigres, três tigres...'

x.replace('tigre', 'pato')
```




    'Um pato, dois patos, três patos...'




```python
# substituir apenas uma vez
x.replace('tigre', 'pato', 1)
```




    'Um pato, dois tigres, três tigres...'




```python
# substituir duas vezes
x.replace('tigre', 'pato', 2)
```




    'Um pato, dois patos, três tigres...'



## Remoção de espaço em branco

- O método `strip` e seus derivados `lstrip` e `rstrip` permitem excluir espações em branco de um texto.


```python
x = '   Lema   '
x
```




    '   Lema   '




```python
x.strip()
```




    'Lema'




```python
x.lstrip()
```




    'Lema   '




```python
x.rstrip()
```




    '   Lema'



<a name='validacao'></a>
# Validação por tipo de conteúdo

## Testar tipos dos objetos
| isdecimal() | isdigit() | isnumeric() |          Example                 |
|:----:|:----:|:----:|:----:|
|    True     |    True   |    True     | "038", "੦੩੮", "０３８"           |
|  False      |    True   |    True     | "⁰³⁸", "⒊⒏", "⓪③⑧"          |
|  False      |  False    |    True     | "↉⅛⅘", "ⅠⅢⅧ", "⑩⑬㊿", "壹貳參"  |
|  False      |  False    |  False      | "abc", "38.0", "-38"             |



```python
n1 = "Ciência"
print(n1.isnumeric())
```

    False



```python
help(str.isdigit)
```

    Help on method_descriptor:
    
    isdigit(self, /)
        Return True if the string is a digit string, False otherwise.
        
        A string is a digit string if all characters in the string are digits and there
        is at least one character in the string.
    



```python
teste1 = '1234'.isdigit()  # numérico?
teste2 = '1234 '.isdigit()  # espaço no fim afeta o tipo?
teste3 = '12.34'.isnumeric()  # ponto decimal?
teste4 = '-10'.isnumeric()
teste5 = 'ABC'.isalpha()  # Alfabético
teste6 = '123a'.isalnum()
```


```python
print(f" {teste1} \t {teste2} \t {teste3} \t {teste4} \n {teste5} \t {teste6}")
```

     True 	 False 	 False 	 False 
     True 	 True



```python
# Outra alternativa para testar o tipo do objeto

help(isinstance)

```

    Help on built-in function isinstance in module builtins:
    
    isinstance(obj, class_or_tuple, /)
        Return whether an object is an instance of a class or of a subclass thereof.
        
        A tuple, as in ``isinstance(x, (A, B, ...))``, may be given as the target to
        check against. This is equivalent to ``isinstance(x, A) or isinstance(x, B)
        or ...`` etc.
    



```python
isinstance("Teste123", str)
```




    True




```python
# Outros testes: maicusculo x minusculo
a = 'ABC'
c = 'abc'
```


```python
a.isupper()
```




    True




```python
a.islower()
```




    False




```python
c.islower()
```




    True



<a name='formatacao'></a>
# Formatação de string

## Exemplos inciais


```python
"{} {}".format("Hello", "World")
```




    'Hello World'




```python
"{}: {} x {} = R$ {}".format("Receita", 1.50, 10, 15.0)
```




    'Receita: 1.5 x 10 = R$ 15.0'




```python
"{0} {1} {0}".format("--", "Ufpb")
```




    '-- Ufpb --'



## Largura da string e posição/alinhamento


```python
# Especificar largura
"{0} {1:10} {0}".format("--", "Ufpb")
```




    '-- Ufpb       --'




```python
# Especificar largura e posição: centralizado
"{0} {1:^10} {0}".format("--", "Ufpb")
```




    '--    Ufpb    --'




```python
# Especificar largura e posição: esq.
"{0} {1:<10} {0}".format("--", "Ufpb")
```




    '-- Ufpb       --'




```python
# Especificar largura e posição: dir.
"{0} {1:>10} {0}".format("--", "Ufpb")
```




    '--       Ufpb --'



## Parâmetros como listas ou dicionários

- E se o parâmetro a ser formatado for uma lista?


```python
# lista
"{0[0]} --- {0[1]}".format(['123', '456'])
```




    '123 --- 456'



## Parâmetros como listas ou dicionários

- E se o parâmetro a ser formatado for um dicionário?


```python
# dicionário

"{0[nome]} --- {0[telefone]}".format({'telefone': 32167453, 'nome': 'Decon'})
```




    'Decon --- 32167453'



## Formatação de números

### Zero à esquerda 


```python
# zeros à esquerda
"{0:06}".format(1001)
```




    '001001'



### Outros caracteres por diferentes posicionamentos


```python
# inserir outros caracteres à esquerda
"{0:*=15}".format(879833301)
```




    '******879833301'




```python
# inserir outros caracteres distribuídos
"{0:*^15}".format(879833301)
```




    '***879833301***'




```python
# inserir outros caracteres distribuídos
"{0:*<15}".format(879833301)
```




    '879833301******'



## Formatação de números

### Formatação com milhar


```python
"{:,.2f}".format(2550)
```




    '2,550.00'



### Forçar a impressão do sinal +


```python
"{:+} {:+}".format(10, -2)
```




    '+10 -2'



### Formatação com percentual


```python
"{:.2%}".format(0.64509)
```




    '64.51%'



## Definição do locale (Brasil)


```python
import locale
locale.setlocale(locale.LC_ALL, "pt_BR.utf-8")
```




    'pt_BR.utf-8'




```python
# Formatação com milhar
"{:n}".format(2550)
```




    '2.550'




```python
# Formatação com milhar e decimal
"{:n}".format(2550.23)
```




    '2.550,23'



<a name='exercicios'> </a>

# Exercício

1- Fazer um programa referente ao **Jogo da Forca**. Contendo os seguintes elementos:
- Palavra secreta (protegida)
- Dica de quantos caracteres tem a palavra digitada
- Máximo de três erros
- Não permitir repetição de letras

                


## Referências

- Chen (2018). *Pandas for everyone: python data analysis* Addison-Wesley Professional.
- Marcondes (2018). *Matemática com Python*. São Paulo: Novatec.
- Menezes (2019). *Introdução à programação com Python*. 3 ed. São Paulo: Novatec.
