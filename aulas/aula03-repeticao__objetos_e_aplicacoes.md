![ds-py-capa.png](attachment:ds-py-capa.png)


# Aula 03 - Repetição, objetos e aplicações


## Objetivo
Apresentar noções gerais de operações com listas e outros objetos `Python`, bem como funções para operações repetitivas `while` e `for`.


## Conteúdo
1. [Repetições (while)](#while) 
2. [Listas](#lista)
3. [Operações com listas](#operacoes)
4. [Dicionários](#dic)
5. [Tuplas](#tupla)
6. [Conjuntos](#set)
7. [Strings](#string)
8. [Repetições (for)](#for)
9. [Exercícios](#exercicios)


<a name="while"></a>
# Repetições (`while`)

- Conhecidas como sequências, cilos, laços ou *loopings*, são de dois tipos no Python: `for` ou `while`.
- **Objetivo**: executar uma mesma tarefa várias vezes.
- Inicialmente, vamos trabalhar apenas com `while`. Principal característica: repete um mesmo bloco enquanto uma condição for verdadeira.

```
while <condição>:
    <bloco>
```
- **Alerta**: Ciclo pode ser infinito enquanto a condição não for falsa ou se não tiver um comando de interrupção.

**Exemplo 01**: faça um programa para escrever a contagem regressiva do réveillon. Últimos 10 segundos!



```python
import time
x = 10
while x>=0:
    time.sleep(1)
    
    if x is 0:
        print('\t\t FELIZ ANO NOVO!')
    else:
        print(f'\t\t\t{x}')
        
    x-=1
    
# Nesse caso, a variável x atua como um contador
```

    			10
    			9
    			8
    			7
    			6
    			5
    			4
    			3
    			2
    			1
    		 FELIZ ANO NOVO!


## Exemplo 02

Fazer um programa para imprimir apenas números ímpares, com valores de entrada inteiros onde o usuário informa o valor do final da sequência.


```python
x_1 = int(input('Digite o último número a imprimir: '))
x = 0

while x<=x_1:
    if (x % 2) is not 0:
        print(x)
    x += 1
```

    Digite o último número a imprimir: 5
    1
    3
    5


## Acumuladores

A diferença entre um contador (o caso de `x` nos exemplos anteriores) e um acumulador, é que o segundo o valor adicionado é variável

**Exemplo 03**: Fazer um programa para exibir a média de três notas dos alunos da disciplina `DS-PY` na UFPB.


```python
n = 1 
soma = 0

while n<=3:
    nota = float(input(f'Nota da {n}ª prova: '))
    soma += nota
    n += 1

print(f'A média das notas foi de {soma/3:.1f}')


```

    Nota da 1ª prova: 10
    Nota da 2ª prova: 3
    Nota da 3ª prova: 2
    A média das notas foi de 5.0


## Interropendo a repetição

Podemos inserir a instrução `break` dentro da estrutura `while` para interromper a sua execução, independentemente do contador, acumulador ou outro controle.

**Exemplo 04**: Fazer um programa para exibir a média de qualquer quantidade de valores digitados por um usuário, com a opção de terminar a declaração a qualquer momento.


```python
n = 0 
soma = 0

while True:
    entrada = input(f'Digite um {n+1}º número ou qualquer letra para sair: ')
    if entrada.isalpha() == True:
        print('\n Até a próxima!')
        break
    else:
        soma += float(entrada)
        n += 1

if n is not 0:
    print(f'A média dos valores digitados foi de {soma/n:.1f}')


```

    Digite um 1º número ou qualquer letra para sair: 10
    Digite um 2º número ou qualquer letra para sair: 3
    Digite um 3º número ou qualquer letra para sair: 1
    Digite um 4º número ou qualquer letra para sair: a
    
     Até a próxima!
    A média dos valores digitados foi de 4.7


## Repetições aninhadas

É possível combinar múltilplos `while`, permitindo duas ou mais variáveis com incrementos/decrementos.

**Exemplo 05**: Fazer um programa de tabuada de multiplicação de 1 a 10.



```python
tabuada = 1
while tabuada <= 10:
    número = 0
    while número <= 10:
        print(f"{tabuada:2d} x {número:2d} = {tabuada * número:>3d}")
        número += 1
        if número == 11:
            print("--------------")
    tabuada += 1
    
```

     1 x  0 =   0
     1 x  1 =   1
     1 x  2 =   2
     1 x  3 =   3
     1 x  4 =   4
     1 x  5 =   5
     1 x  6 =   6
     1 x  7 =   7
     1 x  8 =   8
     1 x  9 =   9
     1 x 10 =  10
    --------------
     2 x  0 =   0
     2 x  1 =   2
     2 x  2 =   4
     2 x  3 =   6
     2 x  4 =   8
     2 x  5 =  10
     2 x  6 =  12
     2 x  7 =  14
     2 x  8 =  16
     2 x  9 =  18
     2 x 10 =  20
    --------------
     3 x  0 =   0
     3 x  1 =   3
     3 x  2 =   6
     3 x  3 =   9
     3 x  4 =  12
     3 x  5 =  15
     3 x  6 =  18
     3 x  7 =  21
     3 x  8 =  24
     3 x  9 =  27
     3 x 10 =  30
    --------------
     4 x  0 =   0
     4 x  1 =   4
     4 x  2 =   8
     4 x  3 =  12
     4 x  4 =  16
     4 x  5 =  20
     4 x  6 =  24
     4 x  7 =  28
     4 x  8 =  32
     4 x  9 =  36
     4 x 10 =  40
    --------------
     5 x  0 =   0
     5 x  1 =   5
     5 x  2 =  10
     5 x  3 =  15
     5 x  4 =  20
     5 x  5 =  25
     5 x  6 =  30
     5 x  7 =  35
     5 x  8 =  40
     5 x  9 =  45
     5 x 10 =  50
    --------------
     6 x  0 =   0
     6 x  1 =   6
     6 x  2 =  12
     6 x  3 =  18
     6 x  4 =  24
     6 x  5 =  30
     6 x  6 =  36
     6 x  7 =  42
     6 x  8 =  48
     6 x  9 =  54
     6 x 10 =  60
    --------------
     7 x  0 =   0
     7 x  1 =   7
     7 x  2 =  14
     7 x  3 =  21
     7 x  4 =  28
     7 x  5 =  35
     7 x  6 =  42
     7 x  7 =  49
     7 x  8 =  56
     7 x  9 =  63
     7 x 10 =  70
    --------------
     8 x  0 =   0
     8 x  1 =   8
     8 x  2 =  16
     8 x  3 =  24
     8 x  4 =  32
     8 x  5 =  40
     8 x  6 =  48
     8 x  7 =  56
     8 x  8 =  64
     8 x  9 =  72
     8 x 10 =  80
    --------------
     9 x  0 =   0
     9 x  1 =   9
     9 x  2 =  18
     9 x  3 =  27
     9 x  4 =  36
     9 x  5 =  45
     9 x  6 =  54
     9 x  7 =  63
     9 x  8 =  72
     9 x  9 =  81
     9 x 10 =  90
    --------------
    10 x  0 =   0
    10 x  1 =  10
    10 x  2 =  20
    10 x  3 =  30
    10 x  4 =  40
    10 x  5 =  50
    10 x  6 =  60
    10 x  7 =  70
    10 x  8 =  80
    10 x  9 =  90
    10 x 10 = 100
    --------------


<a name="lista"></a>
# Listas

Tipo de variável que possibilita armazenar vários valores, acessados por um índice.

`
L = [ ]
`

- Uma lista pode conter zero ou mais elementos de um mesmo tipo.
- Pode conter diversidades de tipos, por exemplo: textos e números
- Uma lista pode conter outras listas
- Uma lista pode ser pensada como um projeto de um *edifício*: em um projeto de 6 pavimentos, teremos andares do térreo (0) ao 5º andar.
- Como está em fase de projeto, o edifício (como uma lista) pode ter a flexibilidade crescer ou diminuir de tamanho.


```python
help(list)
```

    Help on class list in module builtins:
    
    class list(object)
     |  list(iterable=(), /)
     |  
     |  Built-in mutable sequence.
     |  
     |  If no argument is given, the constructor creates a new empty list.
     |  The argument must be an iterable if specified.
     |  
     |  Methods defined here:
     |  
     |  __add__(self, value, /)
     |      Return self+value.
     |  
     |  __contains__(self, key, /)
     |      Return key in self.
     |  
     |  __delitem__(self, key, /)
     |      Delete self[key].
     |  
     |  __eq__(self, value, /)
     |      Return self==value.
     |  
     |  __ge__(self, value, /)
     |      Return self>=value.
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __getitem__(...)
     |      x.__getitem__(y) <==> x[y]
     |  
     |  __gt__(self, value, /)
     |      Return self>value.
     |  
     |  __iadd__(self, value, /)
     |      Implement self+=value.
     |  
     |  __imul__(self, value, /)
     |      Implement self*=value.
     |  
     |  __init__(self, /, *args, **kwargs)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __le__(self, value, /)
     |      Return self<=value.
     |  
     |  __len__(self, /)
     |      Return len(self).
     |  
     |  __lt__(self, value, /)
     |      Return self<value.
     |  
     |  __mul__(self, value, /)
     |      Return self*value.
     |  
     |  __ne__(self, value, /)
     |      Return self!=value.
     |  
     |  __repr__(self, /)
     |      Return repr(self).
     |  
     |  __reversed__(self, /)
     |      Return a reverse iterator over the list.
     |  
     |  __rmul__(self, value, /)
     |      Return value*self.
     |  
     |  __setitem__(self, key, value, /)
     |      Set self[key] to value.
     |  
     |  __sizeof__(self, /)
     |      Return the size of the list in memory, in bytes.
     |  
     |  append(self, object, /)
     |      Append object to the end of the list.
     |  
     |  clear(self, /)
     |      Remove all items from list.
     |  
     |  copy(self, /)
     |      Return a shallow copy of the list.
     |  
     |  count(self, value, /)
     |      Return number of occurrences of value.
     |  
     |  extend(self, iterable, /)
     |      Extend list by appending elements from the iterable.
     |  
     |  index(self, value, start=0, stop=9223372036854775807, /)
     |      Return first index of value.
     |      
     |      Raises ValueError if the value is not present.
     |  
     |  insert(self, index, object, /)
     |      Insert object before index.
     |  
     |  pop(self, index=-1, /)
     |      Remove and return item at index (default last).
     |      
     |      Raises IndexError if list is empty or index is out of range.
     |  
     |  remove(self, value, /)
     |      Remove first occurrence of value.
     |      
     |      Raises ValueError if the value is not present.
     |  
     |  reverse(self, /)
     |      Reverse *IN PLACE*.
     |  
     |  sort(self, /, *, key=None, reverse=False)
     |      Stable sort *IN PLACE*.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes defined here:
     |  
     |  __hash__ = None
    



```python
x = [10, 2, 8] # Lista cujos elementos são números
print(type(x), x)
```

    <class 'list'> [10, 2, 8]



```python
x[0]
```




    10




```python
x[1]
```




    2




```python
x[2]
```




    8




```python
x[-1]
```




    8




```python
x[4]
```


    ---------------------------------------------------------------------------

    IndexError                                Traceback (most recent call last)

    <ipython-input-12-cedd7ac36334> in <module>
    ----> 1 x[4]
    

    IndexError: list index out of range



```python
x = [10, 2, 8, 'Pizza'] # Listas cujos elementos são números e textos
print(x)
```

    [10, 2, 8, 'Pizza']



```python
y = [3, 1, 2, 'Refrigerante']
z = [x, y] # Listas cujos elementos são números, textos e outras listas
print(z)
```

    [[10, 2, 8, 'Pizza'], [3, 1, 2, 'Refrigerante']]



```python
z[0]
```




    [10, 2, 8, 'Pizza']




```python
z[0][3]
```




    'Pizza'




```python
# Listas aninhadas cujos elementos são números, textos, tuplas e listas
lista = [0,1,'a',(1,3),[1,6],[(1,3),'a',1],[1,[(2,3)]]]
print(type(lista), lista)
```

    <class 'list'> [0, 1, 'a', (1, 3), [1, 6], [(1, 3), 'a', 1], [1, [(2, 3)]]]


## Exemplo 06

Calcule a média salarial de um vendedor nos últimos seis meses:

|Período|Salário|
|:---:|:---:|
|1|1000|
|2|1600|
|3|3000|
|4|2000|
|5|1100|
|6|1200|



```python
# Resposta 1: Modo ineficiente, mas possível de execução

w = [1000, 1600, 3000, 2000, 1100, 1200]
media = (w[0] + w[1] + w[2] + w[3] + w[4] + w[5])/6

print(f'A média salarial é de R$ {media:.2f}')
```

    A média salarial é de R$ 1650.00



```python
# Resposta 2: Usando while

w = [1000, 1600, 3000, 2000, 1100, 1200]
i = 0
soma = 0

while i <=5:
    soma += w[i]
    i += 1

print(f'A média salarial é de R$ {soma/i:.2f}')
```

    A média salarial é de R$ 1650.00


<a name="operacoes"></a>
# Operações com listas

## Cópia e fatiamento (subset)

- No `Python`, uma cópia de uma lista pode ser com ou sem vinculação

### Cópia de lista com vinculação


```python
# Cópia vinculada
x = [1, 2, 3, 4, 5]
y = x
y
```




    [1, 2, 3, 4, 5]




```python
# Mudança de conteúdo em y modifica também x

y[0] = 6
print(f' vetor y {y} é igual ao \n vetor x {x}')

# no Python, o objeto cópia funciona como um apelido do objeto original
```

     vetor y [6, 2, 3, 4, 5] é igual ao 
     vetor x [6, 2, 3, 4, 5]


### Cópia de lista sem vinculação




```python
# Cópia independente
x = [1, 2, 3, 4, 5]
y = x[:]
y[0] = 6
print(f' vetor y {y} é diferente do \n vetor x {x}')
```

     vetor y [6, 2, 3, 4, 5] é diferente do 
     vetor x [1, 2, 3, 4, 5]


- Ao escrever x[:] estamos criando uma nova cópia de x em **áreas diferentes da memória**.

### Fatiamento




```python
x = [1, 2, 3, 4, 5]
x[0:5] # do primiro a menos que o 6º elemento
```




    [1, 2, 3, 4, 5]




```python
x[:4] # elementos de índice menores que o 5º elemento
```




    [1, 2, 3, 4]




```python
x[1:3] # do segundo a menos que o 4º elemento
```




    [2, 3]




```python
x[3:] # do quarto elemento em diante
```




    [4, 5]



#### Fatiamento com índices negativos

- `x[0]` é o 1º elemento, mas com índice negativo temos que `x[-1]` é o último elemento



```python
x[-2] # o penúltimo elemento
```




    4




```python
x[-2:] # sinal negativo inverte a posição do índice
```




    [4, 5]




```python
x[::2] # valores alternados
```




    [1, 3, 5]



## Tamanho de listas (lenght)

```
x = [1, 2, 4]
len(x) # =>> 3 elemntos
```

```
x = []
len(x) # =>> 0 elemntos
```

### Exemplo 07

- Faça um programa para fazer uma soma acumulada de um vetor: com controle fixo e flexível: `x = [1, 2, 4]`



```python
# while "FIXO"
x = [1, 2, 4]
i = 0
soma = 0
while i < 3:
    soma += x[i]
    i += 1
soma
```




    7




```python
# while "LEN" => mais flexível. Qual é a vantagem?
i = 0
soma = 0
while i < len(x):
    soma += x[i]
    i += 1
soma
```




    7



## Operações matemáticas com elementos numéricos de uma lista

- Usando as funções `min()`, `max()` e `sum()`. 
- **Observação**: também funciona com tuplas cujos elementos são números.


```python
l = [1,4,3,2,1,0,-3,-1]

# Calcular o valor mínimo entre os elementos numéricos
mínimo = min(l)
máximo = max(l)
soma = sum(l)

print(f'mínimo = {mínimo}, máximo = {máximo}, soma = {soma}')
```

    mínimo = -3, máximo = 4, soma = 7


## Adição elementos

- As funções `append`, `insert`, e `extend` permitem adicionar elementos a uma lista existente


```python
x = []
x.append('A')
x
```




    ['A']




```python
x.append(['C', 'D'])
x.extend(['E', 'F'])
x
```




    ['A', ['C', 'D'], 'E', 'F']




```python
x.insert(1, 'B') # adição com posição definida
x
```




    ['A', 'B', ['C', 'D'], 'E', 'F']




```python
# Concatenando listas com o operador +

l1 = ["banana", "uva"]
l2 = ["laranja", "limão"]

print(l1 + l2)      

```

    ['banana', 'uva', 'laranja', 'limão']


## Remoção de elementos

- Para retirar elementos de uma lista, podemos usar a função `del`, os método `remove` e `clear`.
- A função `range` gera uma sequência de números, que podem ser transformados em lista.


```python
x = ['A', 'B', 'C', 'D', 'A']
x0 = x[:]
del x[1] # exclusão do 2º elemento
print(f' Objeto inicial {x0} \n Objeto final {x}')
```

     Objeto inicial ['A', 'B', 'C', 'D', 'A'] 
     Objeto final ['A', 'C', 'D', 'A']



```python
x = list(range(11))
x0 = x[:]
del x[1:8]
print(f' Objeto inicial {x0} \n Objeto final {x}')
```

     Objeto inicial [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] 
     Objeto final [0, 8, 9, 10]



```python
# Exclusão usando remove (Eliminação da 1ª aparição de um elemento na lista)
x = ['A', 'B', 'C', 'D', 'A']
x.remove('A')
x
```




    ['B', 'C', 'D', 'A']




```python
# o método clear remove todos os elementos
x.clear()
x
```




    []



## Ordenamento

- `sort`: ordena a lista com alteração de seus elementos
- `sorted`: ordena a lista sem alteração de seus elementos (cria-se uma nova lista)


```python
help(sorted)
```

    Help on built-in function sorted in module builtins:
    
    sorted(iterable, /, *, key=None, reverse=False)
        Return a new list containing all items from the iterable in ascending order.
        
        A custom key function can be supplied to customize the sort order, and the
        reverse flag can be set to request the result in descending order.
    



```python
# Exemplo
x = [5, 1, 3]
x.sort(reverse=True) # x.reverse()
x
```




    [5, 3, 1]




```python
#Criação de uma nova lista com ordenamento independente da original
x = [5, 1, 3]
y = sorted(x, reverse=True)
print(f' Objeto inicial {x} \n Objeto final   {y}')
```

     Objeto inicial [5, 1, 3] 
     Objeto final   [5, 3, 1]


## Usando estruturas de decisão com objetos tuplas e listas

**Exemplo**: Como verificar se um item pertence a uma lista de compras?


```python
lista = ["banana", "leite", "carne"]
#item = "banana"
item = "morango"

if item in lista:
    print("O item %s está na minha lista de compras."%item)
else:
    print("O item %s não está na minha lista de compras."%item)
    
# Se o elemento não pertence ao objeto

```

    O item morango não está na minha lista de compras.



```python
tupla = 3,4,5
v = 4
# Usando como regra de decisão

if v in tupla:
    print("O valor %d foi encontrado!"% v)
else:
    print("O valor %d não foi encontrado!"%v)
```

    O valor 4 foi encontrado!


<a name="tupla"></a>
# Tuplas

- Uma tupla é uma coleção de objetos separados por vírgula. Ela é delimitada por `( )`.
- As tuplas podem heterogêneas e formadas por diferentes tipos de objetos (texto, número).
- As tuplas também podem ser aninhadas (agrupadas), ou seja, tuplas cujos elementos também são tuplas.
- uma tupla é um **objeto imutável**, isto é, não pode ser alterada após ser criada. Essa é a principal diferença de uma **lista**.

**Exemplos**:


```python
v = (1, 3, 4)
type(v)
```




    tuple



## Tupla com um único elemento 

- *Sintaxe*: `(elemento, )`

Exemplo


```python
v = 'a',
print(v, type(v), sep = "\n")
```

    ('a',)
    <class 'tuple'>



```python
# Observe a diferença
v = ('a')
print(v, type(v), sep = "\n")
```

    a
    <class 'str'>


## Tupla cujos elementos são números e textos



```python
t = (1,2,'a','b')
print(t, type(t), sep = '\n')

```

    (1, 2, 'a', 'b')
    <class 'tuple'>



```python
## Observe a construção alternativa sem ( ) e com vírgulas
r = 1,2,'a','b'
print(r, type(r), sep = '\n')
```

    (1, 2, 'a', 'b')
    <class 'tuple'>


## Agrupamento de tuplas (aninhamento)

- Uma tupla pode conter outra ou mais tuplas (ou outros objetos, como listas).


```python
produto = ('banana', 'maça', 'milho'), 

lojas = 'A', 'B', 'C', 'D', 'E',

uf = 'PB', None # None representa nada ou nenhum valor

agrupada = produto, lojas, uf

print(agrupada)
```

    ((('banana', 'maça', 'milho'),), ('A', 'B', 'C', 'D', 'E'), ('PB', None))


## Acessando elementos

- A consultar de elementos de uma tupla segue o mesmo padrão do caso das listas.


```python
produto = ('banana', 'maçã', 'milho')

### Elemento na posição 0
produto[0]
```




    'banana'




```python
produto = ('banana', 'maçã', 'milho')

### Elemento da última posição
produto[-1]
```




    'milho'



## Empacotar e Desempacotar valores

- Essa é uma das características interessantes de uma tupla.


```python
# EMPACOTAMENTO
produto = 'banana', 'maçã', 'milho' 
produto
```




    ('banana', 'maçã', 'milho')




```python
# DESEMPACOTAMENTO
a, b, c = produto
print(f'a={a}, b={b}, c={c}')
```

    a=banana, b=maçã, c=milho



```python
# Desempacotar uma tupla aninhada
t = (('a','b'),(1,2,3))
# Os 2 elementos de t são tuplas
x,y = t
print(f'x={x}, y={y}')
```

    x=('a', 'b'), y=(1, 2, 3)


### Outras opções de desempacotamento

- Podemos usar o `*` para indicar vários valores a serem desempacotados?


```python
t = tuple(range(-1,10,2))
t
```




    (-1, 1, 3, 5, 7, 9)




```python
*a, b, c = t #colocar os últimos valores em b e c, inserindo os demais em a
print(f'a={a}, b={b}, c={c}')
```

    a=[-1, 1, 3, 5], b=7, c=9



```python
a, *b = t
print(f'a={a}, b={b}')
```

    a=-1, b=[1, 3, 5, 7, 9]


### Desempacotar valores pode ocorrer em listas?



```python
produto = 'banana', 'maçã', 'milho' 
produto = list(produto)
type(produto)
```




    list




```python
a, b, c = produto
print(f'a={a}, b={b}, c={c}')
```

    a=banana, b=maçã, c=milho


- As operações de desempacotamento também funcionam em **listas**.

## Uma tupla é mutável?


```python
t = (1,3,2)
t[0] = 4
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-27-2e66a7d79c37> in <module>
          1 t = (1,3,2)
    ----> 2 t[0] = 4
    

    TypeError: 'tuple' object does not support item assignment



```python
# Observe a diferença em relação uma lista
t = [1,3,2]
t[0] = 4
print(t)
```

    [4, 3, 2]


### E se uma tupla conter uma lista, é possível existir alteração?


```python
tupla = ('a', ['b', 'c'])
len(tupla)
```




    2




```python
tupla[1].append('d')
tupla
```




    ('a', ['b', 'c', 'd'])



- Nesse caso, a tupla não foi alterada em si, mas sim a lista que estava contida nela.


```python
# Contando elementos de uma tupla.
t = 3,4,'a'
print(len(t))
   
```

<a name="set"></a>
# Conjuntos

- Estrutura de dados que não admite repetições de elementos.
- Conjuntos não mantêm a ordem de seus elementos


```python
a = set()

a.add(1)
a.add(10)
a.add(-5)
a.add("a")
a
```




    {-5, 1, 10, 'a'}




```python
a.add(1) # valores são únicos
a.remove('a')
a
```




    {-5, 1, 10}




```python
x = 1, 1, 2, 3, 1
set(x)
```




    {1, 2, 3}




```python
# Análise de conjunto
a = {1, 2, 5}
b = {3, 2, 4, 8}

a.union(b)
a | b
```




    {1, 2, 3, 4, 5, 8}




```python
a.intersection(b)
a & b
```




    {2}




```python
a.difference(b)
a - b
```




    {1, 5}




```python
a.issubset(b)

1 in x
-1 not in x
```




    True



<a name="dic"></a>
# Dicionários

- Nos dicionários **chaves** são associadas a valores ou a outros objetos (como outros dicionários ou listas).
- Sintaxe de um dicionário

```
{'chave': <valor>}
```

**Exemplo**: Imagine que estamos com a caderneta de notas de uma turma.


```python
notas = {'Ana': 10, 'Maria': 9, 'João': None, 'Maria': 10} # Chaves são únicas
type(notas)
```




    dict



## Consulta de um dicionário

- No dicionário, a pesquisa/fatiamento é feita por meio de chaves e não do índice (como na lista ou tupla)


```python
notas[0]
```


    ---------------------------------------------------------------------------

    KeyError                                  Traceback (most recent call last)

    <ipython-input-60-a2d1739dde39> in <module>
    ----> 1 notas[0]
    

    KeyError: 0



```python
notas['Ana']
```




    10




```python
# Usando o método keys() para acessar as chaves
notas.keys()
```




    dict_keys(['Ana', 'Maria', 'João'])




```python
# Usando o método values() para acessar os valores
notas.values()
```




    dict_values([10, 10, None])



## Dicionários aninhados


```python
notas = {'Ana': {'A1': 10, 'A2': 8, 'A3': 7}, 'Maria': [1,6,10], 'João': (None, None, None)}
type(notas)
```




    dict




```python
notas['Ana']['A1']
```




    10



## Outros métodos para dicionários

### Retornar valor associado a uma chave espefíca - método get()


```python
notas = {'Ana': 10, 'Maria': 9, 'João': None, 'Maria': 10}
notas.get("Maria")
```




    10



### Remover elemento associado a uma chave espefíca - método pop()


```python
notas = {'Ana': 10, 'Maria': 9, 'João': None, 'Maria': 10}
notas.pop("Ana")
print(notas)
```

    {'Maria': 10, 'João': None}


### Atualiza dicionário incluindo novo par {chave: valor} - método update()


```python
notas = {'Ana': 10, 'Maria': 9, 'João': None, 'Maria': 10}
notas.update({'Pedro':9.5, 'Sabrina': 7.8})
print(notas)
```

    {'Ana': 10, 'Maria': 10, 'João': None, 'Pedro': 9.5, 'Sabrina': 7.8}


<a name="for"></a>
# Repetições (`for`)

- Iterando objetos presentes em uma coleção (como listas, tuplas, conjuntos, dicionários, range).
- `for` tem um funcionamento similar a `while`, sendo que o 1º usa elementos diferentes de uma lista
- Sintaxe do `for`

```
for <variável> in <coleção>:
    <bloco de comandos>
```

## Por que usar repetições?

- Vejamos dois casos a seguir, onde elementos de uma lista são impressos usando quatro `print`'s e usando a função `for`.


```python
lista = [2000, 2004, 2008, 2012]

print(lista[0])
print(lista[1])
print(lista[2])
print(lista[3])
```

    2000
    2004
    2008
    2012



```python
for ano in lista:
    print(ano)
```

    2000
    2004
    2008
    2012


## `while` ou `for`?


```python
L = (2000, 2008, 2012)
i = 0
while i < len(L):
    print(L[i])
    i += 1
```

    2000
    2008
    2012



```python
L = (2000, 2008, 2012)
for i in L:
    print(i)
```

    2000
    2008
    2012


- Cada método de repetição tem seu diferencial, o `for` é interessante para casos que se processe elementos de uma lista. Já `while`, quando não sabemos o limite de repetições.

## `For` funciona em tuplas, conjuntos e dicionários?


```python
tupla = (2000, 2008, 2012)
for ano in tupla:
    print(ano)

```

    2000
    2008
    2012



```python
Set = {2000, 2008, 2012}
for ano in Set:
    print(ano)
```

    2000
    2012
    2008



```python
Dict = {'A':2000, 'C':2008, 'D':2012}
for ano in Dict.values():
    print(ano)
```

    2000
    2008
    2012


## Aplicando a classe `range` em `for`


```python
help(range)
```

    Help on class range in module builtins:
    
    class range(object)
     |  range(stop) -> range object
     |  range(start, stop[, step]) -> range object
     |  
     |  Return an object that produces a sequence of integers from start (inclusive)
     |  to stop (exclusive) by step.  range(i, j) produces i, i+1, i+2, ..., j-1.
     |  start defaults to 0, and stop is omitted!  range(4) produces 0, 1, 2, 3.
     |  These are exactly the valid indices for a list of 4 elements.
     |  When step is given, it specifies the increment (or decrement).
     |  
     |  Methods defined here:
     |  
     |  __bool__(self, /)
     |      self != 0
     |  
     |  __contains__(self, key, /)
     |      Return key in self.
     |  
     |  __eq__(self, value, /)
     |      Return self==value.
     |  
     |  __ge__(self, value, /)
     |      Return self>=value.
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __getitem__(self, key, /)
     |      Return self[key].
     |  
     |  __gt__(self, value, /)
     |      Return self>value.
     |  
     |  __hash__(self, /)
     |      Return hash(self).
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __le__(self, value, /)
     |      Return self<=value.
     |  
     |  __len__(self, /)
     |      Return len(self).
     |  
     |  __lt__(self, value, /)
     |      Return self<value.
     |  
     |  __ne__(self, value, /)
     |      Return self!=value.
     |  
     |  __reduce__(...)
     |      Helper for pickle.
     |  
     |  __repr__(self, /)
     |      Return repr(self).
     |  
     |  __reversed__(...)
     |      Return a reverse iterator.
     |  
     |  count(...)
     |      rangeobject.count(value) -> integer -- return number of occurrences of value
     |  
     |  index(...)
     |      rangeobject.index(value) -> integer -- return index of value.
     |      Raise ValueError if the value is not present.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  start
     |  
     |  step
     |  
     |  stop
    


## Exemplo

- Imprimir os anos referentes às eleições municipais a partir de 1996.


```python
#RANGE(START, STOP, STEP)
for j in range(1996, 2021, 4):
    print(j)
```

    1996
    2000
    2004
    2008
    2012
    2016
    2020



```python
#RANGE(START, STOP, STEP)
for j in range(2020, 1995, -4):
    print(j)
```

    2020
    2016
    2012
    2008
    2004
    2000
    1996


## Operações com listas

1. Popular uma lista vazia em looping
2. Identificar o valor máximo de uma lista
3. Lista de compras com preços, quantidades e produtos em listas, exibindo o gasto por produto e a despesa total
4. Função somatório


```python
#1. Popular uma lista vazia
notas = []

# Ciclo na sequência numérica
num_alunos = int(input('Quantidade de alunos: '))
for i in range(num_alunos):
    res = float(input('Digite uma nota: '))
    notas.append(res)
    
print(notas)
```

    Quantidade de alunos: 2
    Digite uma nota: 5
    Digite uma nota: 10
    [5.0, 10.0]



```python
#2. Valor máximo
x = [10, 5, 1, 0, 20, 100, 1, 2]
máx = x[0]
for i in x:
    if i > máx:
        máx = i
print(máx)
```

    100



```python
#3. Lista de compras
preços = [2, 3, 7, 2, 25]
qtd = [1, 3, 2, 3, 5]
produto = ['Arroz', 'Feijão', 'Ovos', 'Leite', 'Carne']

soma = 0
for x in range(len(preços)):
    gasto = preços[x]*qtd[x]
    soma += gasto
    print(f'{produto[x]:10}: {preços[x]:2} x {qtd[x]:2} = {gasto:5}')
    
print(f"""
---------------------------
{'Total':10}:\t\t{soma}
""")    
    
```

    Arroz     :  2 x  1 =     2
    Feijão    :  3 x  3 =     9
    Ovos      :  7 x  2 =    14
    Leite     :  2 x  3 =     6
    Carne     : 25 x  5 =   125
    
    ---------------------------
    Total     :		156
    



```python
# 4. Função somatório
lista = [2, 3, 7, 2, 4]

# Acumulador
soma = 0
for x in lista:
    soma += x
print(soma)    
        
```

    18


## Enumerate

- Caso se deseje trabalhar simultaneamente com o índice e com os elementos de uma lista.
- A função enumerate gera uma `tuple` em que o 1º valor é um índice e o 2º é o elemento.


```python
# Modo tradicional (sem enumerate)
x = [200, 45, 1]
i = 0
for j in x:
    print(f"[{i}] {j}")
    i += 1
```

    [0] 200
    [1] 45
    [2] 1



```python
# Usando Enumerate
x = [200, 45, 1]
for i, j in enumerate(x):
    print(f"[{i}] {j}")
```

    [0] 200
    [1] 45
    [2] 1


## Ciclos for sobre listas

- Podemos trabalhar com listas aninhadas em um looping `for`?


```python
# Criando uma tupla heterogênea
l = [10,30,5,6.5,'a',['d', 3, 4], {'z':3}]
print(l)

# Iterando/percorrendo os elementos
for j in l:
    print(j)

   
```

    [10, 30, 5, 6.5, 'a', ['d', 3, 4], {'z': 3}]
    10
    30
    5
    6.5
    a
    ['d', 3, 4]
    {'z': 3}


## List comprehensions

- É um forma avançada de operar com listas, com integração da função `for`

- Imagine o seguinte vetor:

`x = [1, 2, 4, 5]`

- Como poderíamos elevar cada elemento de `x` ao quadrado?


```python
# Modo 1: usando o FOR convencional
x = [1, 2, 4, 5]
y = []
for i in x:
    y.append(i**2)
print(y)


```

    [1, 4, 16, 25]



```python
# Modo 2: usando compreensão de lista
z = [i**2 for i in x]
print(z)
```

    [1, 4, 16, 25]


### Ciclo aninhado na lista

- Criação de uma lista `x` usando `range`


```python
x = [i for i in range(10)]

print(x)
```

    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


- Criação de tuplas aninhandas em uma lista: $(x_i , x_i^2)$


```python
x = [(x, x**2) for x in range(10)]

print(x)
```

    [(0, 0), (1, 1), (2, 4), (3, 9), (4, 16), (5, 25), (6, 36), (7, 49), (8, 64), (9, 81)]


### Compreensão de lista com `IF`

- Essa função tem a capacidade de filtrar uma lista usando um `if` interno dentro da compreensão.
- Imagine que de um vetor $x = (1, 2, \ldots, 10)$, queremos apenas os valores pares.


```python
x = [i for i in range(1,11) if (i % 2) is 0]

print(x)
```

    [2, 4, 6, 8, 10]


## Ciclos em listas 

- Formando lista de dicionários


```python
frutas = ['bananas', 'uvas', 'morangos']
qtd = [10, 30, 12]

posicao = 0
lista = []
for x in frutas:
    lista.append({x: qtd[posicao]})
    posicao += 1

print(lista)    
```

    [{'bananas': 10}, {'uvas': 30}, {'morangos': 12}]



```python
# Lista de dicionários >> lista de chaves e lista de valores
lista = [{'bananas': 10}, {'uvas': 30}, {'morangos': 12}]
frutas = [] 
qtd = []
for i in lista:
    for v in i.values():
        frutas.append(v)
    for k in i.keys():
        qtd.append(k)
    
print(frutas)
print(qtd)
```

    [10, 30, 12]
    ['bananas', 'uvas', 'morangos']



```python
# Forma aninhada
frutas = [{'fruta': v} for x in lista for v in x.keys()]
qtd = [{'quantidade': v} for x in lista for v in x.values()]
print(frutas)
print(qtd)
```

    [{'fruta': 'bananas'}, {'fruta': 'uvas'}, {'fruta': 'morangos'}]
    [{'quantidade': 10}, {'quantidade': 30}, {'quantidade': 12}]


## Ciclos for em dicionários


```python
# Ciclo for em dicionários
d = {'a':1, 'b':4, 'c':5}

# Iterar valores
for i in d.values():
    print(i)

# Iterar chaves    
for i in d.keys():
    print(i)
    
# Iterar itens
for i in d.items():
    print(i)
    
# Iterar chave, valor
for k, v in d.items():
    print("Chave: ",k, "valor:", v)
    
    
    
# Popular dicionário a partir de listas de mesmo comprimento

preco = [1,5,6]
produto = ['A', 'B', 'C']

posicao = 0
d = {}
for v in preco:
    d.update({produto[posicao]: v})
    posicao += 1

print(d)    

```

    1
    4
    5
    a
    b
    c
    ('a', 1)
    ('b', 4)
    ('c', 5)
    Chave:  a valor: 1
    Chave:  b valor: 4
    Chave:  c valor: 5
    {'A': 1, 'B': 5, 'C': 6}


# Síntese

## Resumo de estruturas de dados básicas do Python

|Característica | Listas | Tuplas | Dicionários | Conjuntos|
|:---:|:---:|:---:|:---:|:---:|
|Alterações| X | | X | X|
|Tamanho variável| X | | X | X|
|Repetição de elementos| X | X | O$^1$ | |
|Consulta por índice numérico| X |X | | |  
|Consulta por chave|  | | X | O$^2$ |

O$^1$ = Apenas de valores, mas as chaves devem ser únicas; 
O$^2$ = No caso, a consulta é direta pelo valor.

<a name='exercicios'> </a>
# Exercícios

1- Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem caso o valor seja inválido e continue pedindo até que o usuário informe um valor válido.

2- Faça um programa que calcule o fatorial de um número inteiro fornecido pelo usuário. Ex.: 5!=5.4.3.2.1=120

3- Faça um Programa que leia 20 números inteiros e armazene-os num vetor. Armazene os números pares no vetor PAR e os números IMPARES no vetor impar. Imprima os dois vetores.

4- Um funcionário de uma empresa recebe aumento salarial anualmente. Esse funcionário foi contratado em 1995, com salário inicial de $R\$ 1.000$

a.	Em 1996, ele recebeu aumento de 1,5\% sobre seu salário inicial;
b.	De 1997 em diante, os aumentos salariais sempre correspondem ao dobro do percentual do ano anterior.

Faça um programa que determine o salário atual desse funcionário. Após concluir isto, altere o programa permitindo que o usuário digite o salário inicial do funcionário.

5- O cardápio de uma lanchonete é o seguinte:

|Produto|   Código|  Preço |
|---|---|---|
|Cachorro Quente| 100|     5,00|
|Misto Quente| 101|     6,00|
|Hambúrguer     | 102|     10,50|
|Cheeseburguer  | 103|     1,30|
|Refrigerante   | 104|     4,00|
|Água mineral   | 105|     3,00|
|Suco natural   | 106|     5,00|

Faça um programa que leia o código dos itens pedidos e as quantidades desejadas. 

- Calcule e mostre o valor a ser pago por item (preço * quantidade) e o total
geral do pedido. 
- Considere que o cliente deve informar quando o pedido deve ser encerrado.

## Referências

- Chen (2018). *Pandas for everyone: python data analysis* Addison-Wesley Professional.
- Marcondes (2018). *Matemática com Python*. São Paulo: Novatec.
- Menezes (2019). *Introdução à programação com Python*. 3 ed. São Paulo: Novatec.
