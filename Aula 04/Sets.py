# Sets

x = {1,2,3}
print(x)
# Convert to set

y = [3,4,4,5]
y = set(y)
print(y)
# Add

y.add(6)
print(y)

# Intersection

print(x & y)

# Union

print(x | y)
