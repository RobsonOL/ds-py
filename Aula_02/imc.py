while True:
    try:
        altura = float(input("Informe sua altura:"))
        break
    except ValueError:
        print("Altura incorreta.")

while True:
    try:
        peso =  float(input("Informe seu peso:"))
        break
    except ValueError:
        print("Peso incorreta.")

imc = round(peso/(altura**2),2)

peso_ideal_inferior = round(18.5*(altura**2),2)
peso_ideal_superior = round(24.9*(altura**2),2)

if imc < 18.5:
    print("Seu imc foi de {} e você está abaixo do peso. Seu peso ideal é entre {} kg e {} kg.".format(imc,peso_ideal_inferior, peso_ideal_superior))
elif imc > 24.9:
    print("Seu imc foi de {} e você está acima do peso. Seu peso ideal é entre {} kg e {} kg.".format(imc, peso_ideal_inferior, peso_ideal_superior))
else:
    print("Seu imc foi de {} e você está no seu peso ideal.".format(imc))