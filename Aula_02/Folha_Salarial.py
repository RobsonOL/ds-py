# INSS = 8%
# IR = 11%
# Sindicato = 5%

class FolhaPagamento:

    '''Classe que constroe a folha de pagamento de um empregado.'''

    aliquota_ir = 0.11
    aliquota_sindicato = 0.05
    aliquota_inss = 0.08

    def __init__(self):
        self.nome = input("Informe o nome do empregado: ")

        while True:
            try:
                self.salario_hora = float(input("Informe seu salário-hora: R$ "))
                break
            except ValueError:
                print("Valor incorreto.")

        while True:
            try:
                self.horas_trabalhadas = float(input("Informe o número de horas trabalhadas: "))
                break
            except ValueError:
                print("Valor incorreto.")

# Salário bruto

        self.salario_bruto = self.salario_hora*self.horas_trabalhadas

# Primeira dedução é a do INSS

        self.salario_inss = self.salario_bruto*(1 - self.aliquota_inss)

# Segunda dedução é o IR

        self.salario_ir = self.salario_inss*(1 - self.aliquota_ir)

# Terceira dedução é a do sindicato

        self.salario_liquido = self.salario_ir*(1 - self.aliquota_sindicato)

    def Relatorio(self):
        print('_______________________________________')
        print('Folha de Pagamento: \n\nEmpregado: {} \nSalário Bruto: R$ {} \nSalário Líquido: R$ {}' .format(self.nome, self.salario_bruto, self.salario_liquido))


joao = FolhaPagamento()

joao.Relatorio()